# Changes the UI if desired  
#$a = (Get-Host).UI.RawUI
#$a.BackgroundColor = "Black" # backgroud color
#$a.ForegroundColor = "Green" # text color

# Disables the close button. 
# All coding and credit goes to  greg zakharov
# Source http://poshcode.org/4059
# Begin close button code 
$code = @'
using System;
using System.Runtime.InteropServices;

namespace CloseButtonToggle {
  internal static class WinAPI {
    [DllImport("kernel32.dll")]
    internal static extern IntPtr GetConsoleWindow();

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool DeleteMenu(IntPtr hMenu,
                           uint uPosition, uint uFlags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool DrawMenuBar(IntPtr hWnd);

    [DllImport("user32.dll")]
    internal static extern IntPtr GetSystemMenu(IntPtr hWnd,
               [MarshalAs(UnmanagedType.Bool)]bool bRevert);

    const uint SC_CLOSE     = 0xf060;
    const uint MF_BYCOMMAND = 0;

    internal static void ChangeCurrentState(bool state) {
      IntPtr hMenu = GetSystemMenu(GetConsoleWindow(), state);
      DeleteMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
      DrawMenuBar(GetConsoleWindow());
    }
  }

  public static class Status {
    public static void Disable() {
      WinAPI.ChangeCurrentState(false); //its 'true' if need to enable
    }
  }
}
'@

Add-Type $code
[CloseButtonToggle.Status]::Disable()
# End close button code 

###### Connect to O365 ######

$sessonCheck = Get-PSSession
$existingSession = $TRUE

# Checks for an existing O365 session
foreach ($item in $sessonCheck) {
	if ($item.ComputerName -eq "outlook.office365.com"){
		$existingSession = $FALSE
	}
}

if ($existingSession){
	#Get user's credentials and opens the connection
	Try{
		$UserCredential = Get-Credential
		$Session = New-PSSession -ErrorAction Stop -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
	} Catch {
		Write-Host "Authentication Failed. Be sure to include the @liberty.edu on your username.`n"
		Write-Host "Press any key to continue ..."
		$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		exit
	}
	# Imports the Exchange module
	Try{
		Import-PSSession $Session
	} Catch {
		Write-Host "Exchange Powershell module already imported"
		Write-Host "Press any key to continue ..."
		$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
	}
}else{
	Write-Host "Connection to Office356 already exists"
	Write-Host "Press any key to continue ..."
	$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

###### Functions ######

# Function display the main menu screen and allows the user to select the specific action desired.
# Function is initiated via the main method.
Function displayMainMenu {
	While (1){
		clear
		Write-Host   "##################################################"
		Write-Host   "#                Main  Menu        Version 1.1   #"
		Write-Host   "##################################################"
		Write-Host "`n#     0. Get Mailbox Information                 #"
		Write-Host "`n#     1. Get Mailbox Permissions                 #"   
		Write-Host "`n#     2. Manage Mailbox Permissions              #" 
		Write-Host "`n#     3. View Aliases                            #"
		Write-Host "`n#     4. Get Members of Distribution Group       #"
		Write-Host "`n#     5. Manage calendar permissions             #"
		Write-host "`n#     X. Exit                                    #"
		Write-Host "`n##################################################"
		
		# Reads input and delete any whitespace
		$userInput = $host.UI.RawUI.ReadKey("IncludeKeyDown")
		$userInput = $userInput.Character
		
		# Switch to execute selection
		switch ($userInput)
			{
				0 {displayMailboxInfo}
				1 {getMailboxPermission}
				2 {managePermissions}
				3 {getAliases}
				4 {getDGMebers}
				5 {manageCalendarPermissions}
				default {displayMainMenu}
				X {endSession}
			}
	}
}

# Function displays informaiton about a mailbox. Initally uses the Get-MailboxStatistics commandlet
# to display general mailbox info then promts incase the user wants to see the info returend by the 
# Get-Mailbox commandlet. Function is initiated via the displayMainMenu function.
Function displayMailboxInfo {
	clear
	$userInput = Read-Host "Enter the name of the mailbox"
	if ($userInput -eq "m") {displayMainMenu}
	clear
	Try{
		#Alerts tech to the fact that the primary SMTP address is not on the @liberty.edu domain then shows the results of the Get-MailboxStatistics commandlet
		$domainTester = Get-Mailbox $userInput -ErrorAction Stop | Select-Object -ExpandProperty PrimarySMTPAddress
		Get-MailboxStatistics -ErrorAction Stop $userInput
		if ($domainTester -notlike "*@liberty.edu"){
			Write-Host "`nPlease note that the mailbox's primary SMTP address is not '@liberty.edu'"
		}
		# Code for jcharlton2
		if ($userInput -eq "jcharlton2" -or $userInput -eq "jcharlton2@liberty.edu"){
			Write-Host "`nhttps://youtu.be/WDpipB4yehk?t=2m1s`n"
		}
		
		#Allows tech to see more detail form the Get-Mailbox commandlet
		Write-Host "`n`nPress (m) for additional informaiton otherwise press any key to continue..."
		$moreTest = $host.UI.RawUI.ReadKey("IncludeKeyDown")
		$moreTest = $moreTest.Character
		if($moreTest -eq "m") {
			Get-Mailbox -ErrorAction Stop $userInput
		}
	} Catch {
		# Adds the @liberty.edu if the previous commands fail then re-runs the commands
		#######OPTOMIZE THIS CAUSE ITS UGLY ###########
		Try {
			$userInput = $userInput + "@liberty.edu"	
			#Alerts tech to the fact that the primary SMTP address is not on the @liberty.edu domain
			$domainTester = Get-Mailbox $userInput -ErrorAction Stop | Select-Object -ExpandProperty PrimarySMTPAddress
			if ($domainTester -notlike "*@liberty.edu"){
				Write-Host "`nPlease note that the mailbox's primary SMTP address is not '@liberty.edu'"
			}
			
			Get-MailboxStatistics -ErrorAction Stop $userInput
			Write-Host "`nPlease note that " $userInput " is an alias of " $domainTester
			#Allows tech to see more detail if they want it
			Write-Host "`n`nPress (m) for additional informaiton otherwise press any key to continue..."
			$moreTest = $host.UI.RawUI.ReadKey("IncludeKeyDown")
			$moreTest = $moreTest.Character
			if($moreTest -eq "m") {
				Get-Mailbox -ErrorAction Stop $userInput
			}
		} Catch {
			Write-Host "Mailbox not found"
		}
	}
	Write-Host "`n`n`nPress any key to continue ..."
	$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

# Function displays all users/groups that have access to the mailbox, with the exception of inherited 
# permissions and "NT AUTHORITY\SELF". Permissions are displayed in the groups Full Access, Send on Behalf, 
# and Send As. Function is initiated via the displayMainMenu function and the managePermissions function. 
Function getMailboxPermission{
		clear
		$userInput = Read-Host "Enter the name of the mailbox"
		if ($userInput -eq "m") {displayMainMenu}
		clear
		Try {
			#Resolves the true name of the mailbox in case input is an alias and throws an
			#error if it doesn't resolve. Also shows the default domain that the mailbox is on
			$mailboxName = Get-Mailbox $userInput -ErrorAction Stop | Select-Object -ExpandProperty PrimarySMTPAddress
			Write-Host "Showing results for the mailbox" $mailboxName
			
			#Lists out full access, read access, and send as permissions.
			#Filters out SIDS for deleted accounts and inherited permissions.
			Write-Host "`nFull Access Permissions"
			Write-Host "-------------------------------------------------`n"
			Get-MailboxPermission $userInput | where {$_.AccessRights -match "FullAccess" -and $_.IsInherited -eq $false -and $_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.User.tostring() -notlike "?-?-?-?*"} | Select-Object -ExpandProperty user

			Write-Host "`n`nSend On Behalf Permission"
			Write-Host "-------------------------------------------------`n"
			Get-Mailbox $userInput | Select-Object -ExpandProperty GrantSendOnBehalfTo

			Write-Host "`n`nSend As Permissions"
			Write-Host "-------------------------------------------------`n"
			Get-RecipientPermission $userInput  | Select-Object -ExpandProperty Trustee
		} Catch [system.exception] {
			Write-Host "Unable to locate that address. Either the mailbox you entered doesn't exist, is still premised, or it was spelled incorrectly"
		}
			
		
		Write-Host "`nPress 'm' to go to the main screen 'p' to return to the permission screen, or any other key to continue."
		$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
		$selection = $selection.Character
		if ($selection -eq "m"){displayMainMenu}
		if ($selection -eq "p"){managePermissions}
}

# Function display the permisions menu which allows the user to select what needs to be done and then 
# executes the necessary commandlet with the exception of "Check Mailbox Permissions", "Main Menu", and 
# "Exit" options which have their own functions. Each switch shows the current permisions before 
# adding/removing access. Function is initiated via the displayMainMenu function and the 
# getMailboxPermission function.
Function managePermissions{
	While (1) {
		clear
		Write-Host   "##################################################"
		Write-Host   "#                Manage Permissions              #"
		Write-Host   "##################################################"
		Write-Host "`n#     0. Full Access Permissions                 #"
		Write-Host "`n#     1. Send As Permissions                     #"   
		Write-Host "`n#     2. Send on Behalf Permissions              #" 
		Write-host "`n#     c. Check Mailbox Permission                #"
		Write-host "`n#     m. Main Menu                               #"
		Write-host "`n#     x. Exit                                    #"
		Write-Host "`n##################################################"
		
		$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
		$selection = $selection.Character
		if ($selection -eq "m") {displayMainMenu}
		switch ($selection){
			#Full access
			0{
				clear
				Write-Host   "##################################################"
				Write-Host   "#           Full Access Permissions              #"
				Write-Host   "##################################################"
				Write-Host "`n#     0. Grant Full Access Permissions           #"
				Write-Host "`n#     1. Remove Full Access Permissions          #"  
				Write-Host "`n##################################################"
			
				$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
				$selection = $selection.Character
				
				# Add full access permissions
				if($selection -eq "0"){
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
				
					Write-Host "`nFull Access Permissions"
					Write-Host "-------------------------------------------------`n"
					Try {
						Get-MailboxPermission -ErrorAction Stop $mailbox | where {$_.AccessRights -match "FullAccess" -and $_.IsInherited -eq $false -and $_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.User.tostring() -notlike "?-?-?-?*"} | Select-Object -ExpandProperty user
						$username = Read-Host "`nEnter the username that you would like to grant access"
						Add-MailboxPermission $mailbox -User $username -AccessRights FullAccess
						Write-Host "`nPermissions have been granted"
					}Catch{
						Write-Host "The mailbox/user could not be located please Try again."
					}
					Write-Host "`nPress any key to continue ..."
					$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				} else {
				#Remove full access permisions
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
					Try {
						Write-Host "`nFull Access Permissions"
						Write-Host "-------------------------------------------------`n"
						Get-MailboxPermission -ErrorAction Stop $mailbox | where {$_.AccessRights -match "FullAccess" -and $_.IsInherited -eq $false -and $_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.User.tostring() -notlike "?-?-?-?*"} | Select-Object -ExpandProperty user
						$username = Read-Host "Enter the username that you would like to remove"
						Remove-MailboxPermission -ErrorAction Stop $mailbox -User $username -AccessRights FullAccess
						Write-Host "`nPermissions have been removed"
					} Catch {
						Write-Host "The mailbox/user could not be located please Try again."
					}
					Write-Host "`nPress any key to continue ..."
					$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				}
			}
			
			#Send As
			1{
				clear
				Write-Host   "##################################################"
				Write-Host   "#           Send As Permissions                  #"
				Write-Host   "##################################################"
				Write-Host "`n#     0. Grant Send As Permissions               #"
				Write-Host "`n#     1. Remove Send As Permissions              #"  
				Write-Host "`n##################################################"
			
				$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
				$selection = $selection.Character
			
				if($selection -eq "0"){
					#Add Send As permissions
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
					Try{
						Write-Host "`nSend As Permissions"
						Write-Host "-------------------------------------------------`n"
						Get-RecipientPermission -ErrorAction Stop $mailbox  | Select-Object -ExpandProperty Trustee
						$username = Read-Host "Enter the username that you would like to grant access"
						Add-RecipientPermission -ErrorAction Stop $mailbox -AccessRights SendAs -Trustee $username
						Write-Host "`nPermissions have been granted"
					} Catch {
						Write-Host "The mailbox/user could not be located please Try again."
					}
						Write-Host "`nPress any key to continue ..."
						$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				} else {
					#Remove Send As
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
					Try{
						Write-Host "`nSend As Permissions"
						Write-Host "-------------------------------------------------`n"
						Get-RecipientPermission -ErrorAction Stop $mailbox  | Select-Object -ExpandProperty Trustee
						$username = Read-Host "Enter the username that you would like to remove"
						Remove-RecipientPermission -ErrorAction Stop $mailbox -AccessRights SendAs -Trustee $username
						Write-Host "`nPermissions have been removed"
					}Catch{
						Write-Host "The mailbox/user could not be located please Try again."
					}
					Write-Host "`nPress any key to continue ..."
					$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				}
			}
			#Send on Behalf
			2{
				clear
				Write-Host   "##################################################"
				Write-Host   "# (Work in Progress) Send on Behalf Permissions  #"
				Write-Host   "##################################################"
				Write-Host "`n#     0. Grant Send on Behalf Permissions        #"
				Write-Host "`n#     1. Remove  Send on Behlaf Permissions      #"  
				Write-Host "`n##################################################"
			
				$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
				$selection = $selection.Character
				if($selection -eq "0"){
					#Add send on behalf 
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
				
					Write-Host "`nSend on Behalf Permissions"
					Write-Host "-------------------------------------------------`n"
					Try{
						$mailbox = Get-Mailbox -ErrorAction Stop $mailbox | Select-Object -ExpandProperty GrantSendOnBehalfTo
						$username = Read-Host "Enter the username that you would like to grant access"
						$mailboxObject = Get-Mailbox -ErrorAction Stop $mailbox
						$userObject= Get-User -ErrorAction Stop $username
						$mailboxObject.GrantSendOnBehalfTo += $userObject.Identity
						Set-Mailbox -ErrorAction Stop $mailbox -GrantSendOnBehalfTo $mailboxObject.GrantSendOnBehalfTo
						Write-Host "`nPermissions have been granted"
					}Catch{
							Write-Host "The mailbox/user could not be located please Try again."
					}
					Write-Host "`nPress any key to continue ..."
					$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				} else {
					#Remove send on behalf
					clear
					$mailbox = Read-Host "Enter the name of the mailbox"
					if ($mailbox -eq "m") {displayMainMenu}
					Try{
						Write-Host "`nSend On Behalf Permissions"
						Write-Host "-------------------------------------------------`n"
						Get-Mailbox -ErrorAction Stop $mailbox | Select-Object -ExpandProperty GrantSendOnBehalfTo
						Write-Host "`nWARNING: this tool removes all Send On Behalf permission from the mailbox. If you wish to remove only one user please use the web interface (link below)."
						Write-Host "`nhttps://outlook.office365.com/ecp/?rfr=Admin_o365&exsvurl=1&mkt=en-US&Realm=liberty.edu&wa=wsignin1.0"
						Write-Host "`nAre you sure you want to continue? (Y) yes (N)no"
						$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
						$selection = $selection.Character
						if($selection -eq "y"){
							Set-Mailbox -ErrorAction Stop $mailbox -GrantSendOnBehalfTo $null
							Write-Host "`nPermissions have been removed"
						}
						if ($selection -eq "n"){
							Write-Host "`nProcess has been cancelled"
						}
					} Catch {
						Write-Host "The mailbox could not be located please Try again."
					}
					Write-Host "`nPress any key to continue ..."
					$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
				} 
			}
			c{getMailboxPermission}
			m{displayMainMenu}
			x{endSession}
			default {displayMainMenu}
		}
	}
}

# Function lists all aliases associated with the email address and shows the primary address.
# Function is initiated via the displayMainMenu function
Function getAliases{
		clear
		$alias = Read-Host "Please enter the full email address"
		if($alias -eq "m"){displayMainMenu}
		clear
		Try{
			$mailbox = Get-Mailbox -ErrorAction stop $alias
			Write-Host "Listing all available email addresses for " $alias "@liberty.edu"
			Write-Host "`nPlease note that " $mailbox.MicrosoftOnlineServicesID " is the actual name of the mailbox."
			Write-Host "-------------------------------------------------`n"
			$mailbox.EmailAddresses
			$mailbox.Alias
		}Catch{
			clear
			Write-Host "Either the alias doesn't exist or you did not include the full email address"
		}
		Write-Host "`nPress any key to continue ..."
		$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

# Work In Progress. Function is intended to take the input of a email address and return what 'type' of 
# address it is, the types being mailbox, alias, distribution list. Function is initiated via the 
# displayMainMenu function.
Function getType{
	clear
	$userInput = Read-Host "Please enter the full email address"
	if($userInput -eq "m"){displayMainMenu}
		Try{
			clear
			$mailItem = Get-Recipient -ErrorAction Stop $userInput
			#First if block is for mailboxes
			if($mailItem.RecipientType -eq "UserMailbox"){
				if($userInput -eq $mailItem.WindowsLiveID){
					Write-Host "This is a mailbox and can be mapped in Outlook, and accessed via OWA, if a user had Full Access permissions to it."
				} else {
					Write-Host "The address" $userInput "is an alias.`nThe actual name of the is mailbox" $mailItem.WindowsLiveID
					Write-Host "It can be mapped in Outlook, and accessed via OWA."
				}
				Write-Host "`nWould you like to access the permissions menu? (y)yes (n)no"
				$permsissionsMenu = $host.UI.RawUI.ReadKey("IncludeKeyDown")
				$permsissionsMenu = $permsissionsMenu.Character
				if ($permsissionsMenu -eq "y"){getMailboxPermission}
			#Second if block is for distribution lists
			} elseif ($mailItem.RecipientType -eq "MailUniversalSecurityGroup"){
				Write-Host "This is a distribution list and can not be mapped in Outlook"
			} else{
			  Write-Host "Not sure what you put in, but I didn't account for this to happen 'cause you found something that is not a mailbox, and not a distribution list."
			}
		} Catch {
			Write-Host "No email address found. Be sure you include the @liberty.edu"
		}
		Write-Host "`nPress any key to continue ..."
		$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

# Function lists the memebers of a distribution list. Function is initiated via 
# the displayMainMenu function
Function getDGMebers {
	clear
		$userInput = Read-Host "Please enter the name of the distribution group"
		if($userInput -eq "m"){displayMainMenu}
		Try{
			$dgObject = Get-DistributionGroup -ErrorAction Stop $userInput 
			Write-Host "`nMembers of" $dgObject.Name
			Write-Host "`n-------------------------------------------------"
			Get-DistributionGroupMember -ErrorAction Stop $userInput | Select-Object -ExpandProperty Name
			
			Write-Host "`nOwners of" $dgObject.Name
			Write-Host "`n-------------------------------------------------"
			$dgObject.Managedby
			
			Write-Host "`nNot the results you were looking for?`nTry adding @liberty.edu to the end of the address."
		} Catch {
			Write-Host "`nDistribution Group not found.`nTry adding or removing the @liberty.edu for better results."
		}
		Write-Host "`n`n`nPress any key to continue ..."
		$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}
# Function displays the calendar permisions menu which allows a user to view, add, and remove 
# calender permisions. Function also contains a nested menu/switch for selecting the level of 
# calendar permisions to be added. Function is initiated via the displayMainMenu function.
Function manageCalendarPermissions{
	While(1){
			clear
			Write-Host   "##################################################"
			Write-Host   "#            Manage Calendar Permissions         #"
			Write-Host   "##################################################"
			Write-Host "`n#     0. View Calendar Permissions               #"
			Write-Host "`n#     1. Add Calendar Permissions                #"
			Write-Host "`n#     2. Remove Calendar Permissions             #"
			Write-host "`n#     m. Main Menu                               #"
			Write-Host "`n##################################################"
			
			$selection = $host.UI.RawUI.ReadKey("IncludeKeyDown")
			$selection = $selection.Character
			clear
			if ($selection -eq "m") {displayMainMenu}
			switch ($selection){
				0{
					$mailbox = Read-Host "Please enter the email address to check their calandar's permissions"
					$mailbox = $mailbox + ":\Calendar"
					Try{
						Get-MailboxFolderPermission -Identity $mailbox -ErrorAction Stop | Select-Object User,AccessRights
					} Catch {
						Write-Host "An error was encountered"
					}
				
				}
				
				1{
					# Menu to select what level of permisions
					$mailbox = Read-Host "Please enter the email address for the calendar you would like to grant access to"
					$addCmdlet = "Add-MailboxFolderPermission -Identity " + $mailbox + ":\Calendar -AccessRights "
					# build big switch
					clear
					Write-Host   "##################################################"
					Write-Host   "#            Permissions Level                   #"
					Write-Host   "##################################################"
					Write-Host "`n#     0. Owner                                   #"
					Write-Host "`n#     1. Publishing Editor                       #"
					Write-Host "`n#     2. Editor                                  #"
					Write-Host "`n#     3. Publishing Author                       #"
					Write-Host "`n#     4. Author                                  #"
					Write-Host "`n#     5. NonEditing Author                       #"
					Write-Host "`n#     6. Reviewer                                #"
					Write-Host "`n#     7. Contributor                             #"
					Write-Host "`n#     8. Availability Only                       #"
					Write-Host "`n#     9. Limited Details                         #"
					Write-Host "`n#     x. Exit                                    #"
					Write-Host "`n##################################################"
					$permission = $host.UI.RawUI.ReadKey("IncludeKeyDown")
					$permission = $permission.Character

					Switch ($permission) {
						0{
							$addCmdlet = $addCmdlet +  "'Owner'"
							Write-Host "$addCmdlet"
						}
					
						1{
							$addCmdlet = $addCmdlet +  "'PublishingEditor'"
						}
						
						2{
							$addCmdlet = $addCmdlet +  "'Editor'"
						}
						
						3{
							$addCmdlet = $addCmdlet +  "'PublishingAuthor'"
						}
						
						4{
							$addCmdlet = $addCmdlet +  "'Author'"
						}
						
						5{
							$addCmdlet = $addCmdlet +  "'NonEditingAuthor'"
						}
						
						6{
							$addCmdlet = $addCmdlet +  "'Reviewer'"
						}
						
						7{
							$addCmdlet = $addCmdlet +  "'Contributor'"
						}
						
						8{
							$addCmdlet = $addCmdlet +  "'AvailabilityOnly'"
						}
						
						9{
							$addCmdlet = $addCmdlet +  "'LimitedDetails'"
						}
						
						X{displayMainMenu}
					}
					$addCmdlet = $addCmdlet + "-ErrorAction Stop"
					Try{
						Invoke-Expression $addCmdlet
						Write-Host "Permission have ben granted."
					} Catch {
						Write-Host "An error was encountered."
					}
				}
				# Removes all calendar permisions for the selected user
				2{
					$mailbox = Read-Host "Please enter the email address for the calendar you would like to remove access"
					$mailbox = $mailbox + ":\Calendar"
					Remove-MailboxFolderPermission $mailbox
					Write-Host "Permission have ben removed."
				}
				
				X{displayMainMenu}
			}
			Write-Host "`nPress 'm' to go to the main screen or any other key to continue."
			$userInput = $host.UI.RawUI.ReadKey("IncludeKeyDown")
			$userInput = $userInput.Character
			if ($userInput -eq "m"){displayMainMenu}
	}
}

Function endSession {
	Remove-PSSession -ComputerName outlook.office365.com
	exit
}

###### Main Method ######
displayMainMenu