﻿<#
    Script: mx_audit
    Author: ???
    Modified: Jorge Torres
    - 8.6.b Disabled account added to script. Cleared syntax on loops.
    - 8.6.c Pointed ActiveDirectory library to DC05 on Set-ADAccountPassword function.
    - 9.0   Added Auto post to UserManager.  Recoded Logic.  
    - 9.1   Corrected Exchange session code.
    - 9.2   Corrected Session code to verify Username.  Corrected code to remove rules and display all inbox rules. 
    - 9.3   Added expire of account on lock-down procedure.
    - 9.4   Change code and remove try/catch.  Added AssemblyType. Change Exchange connection script.
    - 9.5   Corrected try/catch to remove error on Lock-down code.
    - 9.6   Added Security group correction against user loading script.
    Notes:  
    1. If issues happen with script not connecting to Exchange Online, make sure that computer is in the correct OU.  
       Check WinRM for information 
       command prompt to "winrm get winrm/config -format:pretty"
       if Basic authentication is set to False or control by (GPO) then Script will not work.

    2. If the script does not work check execution policy 
       on powershell "get-executionpolicy -list
       LocalMachine should be Unrestricted,
       if not then run the following command
       "Set-ExecutionPolicy -scope LocalMachine unrestricted"
#>


    #region function Exec-Chrome
    function Exec-Chrome
    {	
		[CmdletBinding()]
		Param
		(
			[Parameter(
				Mandatory = $true
				 )
			]
			$URL_Addr
		)
		Begin
		{
		}
		Process
		{
            $RAApp = new-object System.Diagnostics.ProcessStartInfo
            $RAApp.FileName = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
            $RAApp.Arguments = " " + $URL_Addr
            $RAApp.LoadUserProfile = $false
            $RAApp.UseShellExecute = $false
            $RAApp.WorkingDirectory = (get-location).Path
            [System.Diagnostics.Process]::Start($RAApp)  
        }
    }
    #endregion    

    #region function UserManagerPostQuery
    function UserManagerPostQuery($notes)
    {
        $URL = "http://underground.liberty.edu/admin/usermanager.cfm"
        
        $LoginFormData = @{
            "Action"="SecuritySystemLogin"
	        "SecuritySystemLoginUserName"= $Credential.UserName #Insert Global Username
	        "SecuritySystemLoginPassword" = $Credential.GetNetworkCredential().password #Insert Global Password
        }

        $Headers = @{
            "Host"="underground.liberty.edu"
               "Pragma" = "no-cache"
               "Cache-Control" = "no-cache"
               "Origin" = "http://underground.liberty.edu"
               "Upgrade-Insecure-Requests" = "1"
               "Content-Type" = "application/x-www-form-urlencoded"
               "Accept" = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
               "Referer" = "http://underground.liberty.edu/admin/usermanager.cfm"
               "Accept-Encoding"= "gzip, deflate"
               "Accept-Language" = "en-US,en;q=0.8"
        }

        $req = Invoke-WebRequest -Uri $URL -Headers $Headers -Body $LoginFormData -Method POST -SessionVariable UserManager

        $NotesForm = @{
            "action" = "Update"
            "Domain" = "Sensenet"
            "username" = $MXUsername
            "NewAcctNoteText" = $notes 
            "AddNote" = "Add Note"
            "NewGroup" = "ACM"
        }
        $note = Invoke-WebRequest -Uri $URL -Body $NotesForm -Headers $Headers -Method POST -WebSession $UserManager
        if ($note.StatusCode -eq 200)
            {Write-Host "UserManager notes added sucessfully."}
        else
        {
            Write-Host "UserManager notes failed.  Please add note manually."
            Exec-Chrome -URL_Addr "http://underground.liberty.edu/admin/usermanager.cfm"
        }
         Write-Host "**************************"
    }
    #endregion

#region
Function Out-Clipboard
{
    param($Value,[switch]$PassThru) 
    begin 
    {
        [void][reflection.assembly]::LoadWithPartialName("Windows.Forms")
        Add-Type -AssemblyName System.Windows.Forms
        $tb = New-Object System.Windows.Forms.TextBox
        $tb.Multiline = $true
        $pipeObjects = @()
    }
    process 
    {
      $pipeObjects+=$_
    }
    end 
    {
        if([string]::IsNullOrEmpty($Value))
        {
            $text=$null
            $pipeObjects | out-string -stream | %{$text = $text + $(if($text -ne $null){"`r`n"}) + $_}
            $tb.text = $text
        } 
        else 
        {
            $tb.text = $value
        }
        $tb.SelectAll()
        $tb.Copy()
        if($PassThru){
            $pipeObjects
        }
        $tb.Dispose()
    }
}
#endregion

#region function MX_Audit
function MX_Audit
{
    #$username = $Credential.username
    $MXUsername = ""
    $MXMod = 0

    While ($MXUsername -ne "x")
    {
        clear
        Write-Host "MX-Audit $MXAuditVer`n" | Out-Clipboard
        $MXUsername = Read-Host "`nPlease enter the username of the account"
        if ([string]::IsNullOrWhiteSpace($MXUsername))
        {
            #empty username
            Write-Host -ForegroundColor Red "ERROR: Username can not be empty."
        }
        else
        {
            #run script
            if ($MXUsername -ne "x")
            {
                $LDAPUsername = Get-ADUser -LDAPFilter "(sAMAccountName=$MXUsername)"
                if ($LDAPUsername -eq $null)
                    {Write-Host -ForegroundColor Red "ERROR: Username not found in LDAP."}
                else
                {
                    $q = Get-Mailbox $MXUsername -ErrorAction SilentlyContinue
        	        Write-Host "**************************" | clip
                    Write-Host "Retrieving Mailbox Stats" 
                    $OnlineUsername = $MXUsername + "@liberty.edu"
                    $MXOWAPolicy = (Get-CASMailbox -identity $OnlineUsername | Select OWAEnabled)
                    if ($MXOWAPolicy.OWAEnabled -eq $True)
                        {Write-Host "OWA WebAcess: Enabled" }
                    else
                        {Write-Host "OWA WebAcess: Disabled" }
                    $MXSendSize = (Get-Mailbox -identity $OnlineUsername | select MaxSendSize)
                    $MXRec = Get-Mailbox $OnlineUsername | Get-MailboxStatistics | Select DeletedItemCount, ItemCount, TotalDeletedItemSize, TotalItemSize, DisplayName, LastLogonTime

                    $tempStr = $MXRec.DisplayName
                    Write-Host "Display Name: $tempStr" 
                    $tempStr = $MXRec.ItemCount 
                    Write-Host "Total Item count: $tempStr"
                    $tempStr =  $MXRec.TotalItemSize
                    Write-Host "Total Item size: $tempStr" 
                    $tempStr = $MXRec.DeletedItemCount
                    Write-Host "Total Deleted count: $tempStr" 
                    $tempStr = $MXRec.TotalDeletedItemSize
                    Write-Host "Total Deleted size: $tempStr" 
                    $tempStr = $MXRec.LastLogonTime
                    Write-Host "Last Logon Time: $tempStr" 
                    Write-Host "**************************" 
	                Write-Host "Send Block" 
                    $MXSendSize = (Get-Mailbox -identity $OnlineUsername | select MaxSendSize)
                    $tempStr = $MXSendSize.MaxSendSize
                    Write-Host "Max Send Size: $tempStr" 
                    #$MXBlock = (get-mailbox -identity $OnlineUsername | get-MailboxJunkemailConfiguration)
                    #$MXBlockList = $MXBlock.blockedsendersanddomains
                    #Write-Host "Blocked Domain: $MXBlockList"
                    Write-Host "**************************" 
	                $permission = Read-Host "Would you like to add yourself to the mailbox (y/n)" 
	                if ($permission -eq "y")
                    {
		                try
                        {
			                Add-MailboxPermission $MXUsername -User $username -AccessRights FullAccess
			                Write-Host "`nFull Access Permissions have been added."
		                } 
                        catch
                        {
                            Write-Host -ForegroundColor Red "`nERROR: Full Access Permissions were not added."
                        }
	                }
	
                    Write-Host "**************************"
	                Write-Host "`nForwarding address"
                    $fwdAddr = Get-Mailbox $MXUsername | select ForwardingSmtpAddress
                    $tempStr = $fwdAddr.ForwardingSmtpAddress
                    if ($tempStr-eq $null)
                        {Write-Host "ForwardingSmtpAddress: N/A"}
                    else
                    {
                        Write-Host "ForwardingSmtpAddress: $tempStr"
                        $FwdRet = Read-Host "Do you want to remove this forwarding address? (y/n)"
                        if ($FwdRet -eq "y")
                        {
                            $MXMod = 1
                            Set-Mailbox $MXUsername -ForwardingSmtpAddress $null
                        }
                    }
                
                    Write-Host "**************************"
	                Write-Host "Inbox Rules"
                    $MXRule = 0
                    $InRules = Get-InboxRule -Mailbox $MXUsername | Select Enabled, Name, Description
                    #$InRules = Get-InboxRule -Mailbox $MXUsername -WarningAction SilentlyContinue
                    ForEach ($Rule in $InRules) 
                    {
                        $MXRule = 1
                        Write-Host "Name: $($Rule.Name)"
                        Write-Host "Description: $($Rule.Description)"
                        Write-Host "Enabled: $($Rule.Enabled)"
                        Write-Host ""
                    }
                    if ($MXRule -eq 1)
                    { 
                        $InboxRet = Read-Host "Do you want to remove ALL Inbox rules? (y/n)"
                        if ($InboxRet -eq "y")
                        {
                            $MXMod = 1
                            #Get-InboxRule -Mailbox $MXUsername | Remove-InboxRule -mailbox $MXUsername -Identity $RulesDesc #This would remove individual rules inside of the loop
                            Get-InboxRule -Mailbox $MXUsername | Remove-InboxRule -Confirm:$false #removes ALL rules
                        }
                    }
                    Write-Host "**************************"
	                Write-Host "Auto Reply Rules"
                    $AutoReply = Get-MailboxAutoReplyConfiguration $MXUsername | Select AutoReplyState
                    $tempStr = $AutoReply.AutoReplyState

                    if ($tempStr -eq "Disabled")
                        {Write-Host "AutoReply: Disabled"}
                    else 
                    {
                        Write-Host "Autoreply: $tempStr"
                        $AutoRet = Read-Host "Do you want to turn off auto replies? (y/n)"
                        if ($AutoRet -eq "y")
                        {
                            $MXMod = 1
                            Set-MailboxAutoReplyConfiguration $MXUsername -AutoReplyState disabled
                        }
                    }

                    Write-Host "**************************"
	                Write-Host "Signatures"
                    $Signatures = Get-MailboxMessageConfiguration $MXUsername | Select SignatureText, SignatureHTML
                    $tempStr = $($Signatures.SignatureHTML)
                    if (($tempStr -eq $null) -or ($tempStr -eq "@{SignatureText=; SignatureHtml=}"))
                    {
                        Write-Host "HTML Signature: Not Set"
                    }
                    else
                    {
                        Write-Host "HTML Signature:"
                        Write-Host "$TempStr"
                        $AutoRet = Read-Host "Do you want to remove the signatures? (y/n)"
                        if ($AutoRet -eq "y")
                        {
                            $MXMod = 1
                            Set-MailboxMessageConfiguration $MXUsername -SignatureHTML ""
                        }
                    }

                    Write-Host "**************************"
	                if ($permission -eq "y")
                    {
		                $answer = Read-Host "Do you want to remove your permission to this mailbox? (y/n)"
		                if ($answer -eq 'y')
                            {Remove-MailboxPermission $MXUsername -User $username -AccessRights FullAccess}
	                }
                    if ($MXMod -eq 1)
                    {
                        $MXMod = 0 #Reset Mod on the loop
                        $LockdownRet = Read-Host "Do you want to lock-down the account? (y/n)"
                        if ($LockdownRet -eq "y")
                        {
                            if ((Get-ADGroupMember -Identity "RSHelpdeskT2").name -contains $UserName)
                            {
                                $CharacterArray = @("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "&", "#", "!", "$", "*", "%", "?", "(", ")")
                                $x = 0
                                While ($x -lt 24)
                                {
                                    $GenPwd += $CharacterArray | Get-Random
                                    $x++
                                }
                                $GenPwd = ConvertTo-SecureString -String $GenPwd -AsPlainText -Force
                                Set-ADAccountPassword -Identity $MXUserName -NewPassword $GenPwd
                                Write-Host -ForegroundColor Red "Password has been changed."
                                # Expire Account
                                Set-ADAccountExpiration -Identity $MXUserName -TimeSpan -1
                                Write-Host -ForegroundColor Red "Account has been expired."
                                # Disable Account
                                Disable-ADAccount -Identity $MXUserName
                                Write-Host -ForegroundColor Red "Account has been disabled."
                                # Usermanager notes 
                                $TKTText = Read-Host "Enter Service-Now Ticket to reference in UserManager i.e. TKT0000000"                           
                                $UMNotes = "$TKTText - Password reset, account disabled & expired - $Username"
                                UserManagerPostQuery($UMNotes)
                            }
                            else
                            {Write-Host -ForegroundColor Red "ERROR: Please have a T2 lock-down the Account."}
                        }
                    }
                }

                Write-Host "Press any key to continue or x to exit..."
	            $continue = $host.UI.RawUI.ReadKey("IncludeKeyDown")
	            $continue = $continue.Character
	            if ($continue -eq "x")
                {
                    $MXUsername = "x"
                    try
                    {
		                Remove-PSSession -ComputerName outlook.office365.com
			            Write-Host "Session successfully closed..."
                    } 
                    catch 
                        {Write-Host -ForegroundColor Red "ERROR: Session did not close"} 
	                Write-Host "Exiting script..."
                }        
            }
            else
            {Write-Host "Exiting..."}
        } 
    }
}
#endregion

#region function Main
function Main {
<#
    .SYNOPSIS
        The Main function starts the application.
    
    .PARAMETER Commandline
        $Commandline contains the complete argument string passed to the script packager executable.
    
    .NOTES

#>	
	
    Import-Module ActiveDirectory
    $Global:ProgExit = $false
	$Global:Credential = Get-Credential
    $Global:Username = $credential.username
    if (!$Credential.username.contains("@"))
        {$OnlineUsername = $credential.username + "@liberty.edu"}
    else
    {
        $OnlineUsername = $credential.username
        $Username = ($Username).Split("@")[0]
        Write-Host $Username
    }
    $OnlineCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $OnlineUsername, $credential.password

    $Session = New-PSSession -ErrorAction SilentlyContinue -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $OnlineCredential -Authentication Basic -AllowRedirection
	Import-PSSession $Session -AllowClobber

    MX_Audit
}
#endregion

#Start the application
$Global:MXAuditVer = "v9.6"
Main