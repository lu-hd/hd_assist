﻿$username = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\user.txt'
$password = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\pass.txt' | ConvertTo-SecureString
$Cred = New-Object -TypeName System.Management.Automation.PSCredential `
    -ArgumentList $username, $password
Connect-MsolService -Credential $Cred
$UserID = Read-Host 'Username:> '
$UserEmail = $UserID+'@liberty.edu'
(Get-MsolUser -UserPrincipalName $UserEmail).StrongAuthenticationuserdetails
Read-Host -Prompt "Press Enter to continue"
