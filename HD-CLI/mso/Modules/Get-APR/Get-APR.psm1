﻿function Get-APR

{

  param(
    [Parameter(Mandatory=$false)][string]$User
  )

  $username = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\user.txt'
  $password = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\pass.txt' | ConvertTo-SecureString
  $Cred = New-Object -TypeName System.Management.Automation.PSCredential `
      -ArgumentList $username, $password
      Connect-MsolService -Credential $Cred

  if(!($User)){
    $User = Read-Host 'Username:> '
  }
  $UserEmail = $User+'@liberty.edu'
  $Phone = (Get-MsolUser -UserPrincipalName $UserEmail).StrongAuthenticationUserDetails.PhoneNumber
  $Email = (Get-MsolUser -UserPrincipalName $UserEmail).StrongAuthenticationUserDetails.Email
  if (($Phone -eq $null) -and ($Email -eq $null))
  {
    Write-Host "APR is not set."
  }
  else
  {
    Write-Host "APR is set."
  }
  Read-Host -Prompt "Press Enter to continue"

}