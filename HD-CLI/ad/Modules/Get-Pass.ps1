#__/\\\________/\\\__/\\\\\\\\\\\\_________________________/\\\\\\\\\__/\\\______________/\\\\\\\\\\\_
# _\/\\\_______\/\\\_\/\\\////////\\\____________________/\\\////////__\/\\\_____________\/////\\\///__
#  _\/\\\_______\/\\\_\/\\\______\//\\\_________________/\\\/___________\/\\\_________________\/\\\_____
#   _\/\\\\\\\\\\\\\\\_\/\\\_______\/\\\__/\\\\\\\\\\\__/\\\_____________\/\\\_________________\/\\\_____
#    _\/\\\/////////\\\_\/\\\_______\/\\\_\///////////__\/\\\_____________\/\\\_________________\/\\\_____
#     _\/\\\_______\/\\\_\/\\\_______\/\\\_______________\//\\\____________\/\\\_________________\/\\\_____
#      _\/\\\_______\/\\\_\/\\\_______/\\\_________________\///\\\__________\/\\\_________________\/\\\_____
#       _\/\\\_______\/\\\_\/\\\\\\\\\\\\/____________________\////\\\\\\\\\_\/\\\\\\\\\\\\\\\__/\\\\\\\\\\\_
#        _\///________\///__\////////////_________________________\/////////__\///////////////__\///////////__



function Set-Pass #Completed
{

  param(
    [Parameter(Mandatory=$false)]
    [string]$User,

    [Parameter(Mandatory=$false)]
    [switch]$random_pass_flag, #random flag

    [Parameter(Mandatory=$false)]
    [switch]$manual_pass_flag, #manual flag

    [Parameter(Mandatory=$false)]
    [string]$password #Password
  )

  if(!($User)) #Check to see if Username parameter is set
  {
    $User = Read-Host 'Username:> '
  }

  if($random_pass_flag)
  {
  $auto_pass_flag = 'r'
  }

  if($manual_pass_flag)
  {
  $auto_pass_flag = 'm'
  }

  switch ($auto_pass_flag)
  {
    $null #Default
    {
        $date = Get-Date -Format "MMdd"
        $password = "Liberty${date}"
    }
    'r' #Random
    {
        $asci = [char[]]([char]45..[char]58) + ([char[]]([char]97..[char]122)) + [char[]]([char]65..[char]90)
        $password = (1..$(Get-Random -Minimum 9 -Maximum 14) | % {$asci | get-random}) -join ""
    }
    'm' #Manual
    {
        if(!($password)) #If password was not passed, ask for password
        {
            $password = Read-Host 'Password:> '
        }
    }
  }
    Set-ADAccountPassword -Identity $User -NewPassword (ConvertTo-SecureString -AsPlainText '$password' -Force)
}

#Export-ModuleMember -Function Set-Pass
