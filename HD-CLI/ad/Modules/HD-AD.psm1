#__/\\\________/\\\__/\\\\\\\\\\\\_________________________/\\\\\\\\\__/\\\______________/\\\\\\\\\\\_
# _\/\\\_______\/\\\_\/\\\////////\\\____________________/\\\////////__\/\\\_____________\/////\\\///__
#  _\/\\\_______\/\\\_\/\\\______\//\\\_________________/\\\/___________\/\\\_________________\/\\\_____
#   _\/\\\\\\\\\\\\\\\_\/\\\_______\/\\\__/\\\\\\\\\\\__/\\\_____________\/\\\_________________\/\\\_____
#    _\/\\\/////////\\\_\/\\\_______\/\\\_\///////////__\/\\\_____________\/\\\_________________\/\\\_____
#     _\/\\\_______\/\\\_\/\\\_______\/\\\_______________\//\\\____________\/\\\_________________\/\\\_____
#      _\/\\\_______\/\\\_\/\\\_______/\\\_________________\///\\\__________\/\\\_________________\/\\\_____
#		_\/\\\_______\/\\\_\/\\\\\\\\\\\\/____________________\////\\\\\\\\\_\/\\\\\\\\\\\\\\\__/\\\\\\\\\\\_
#		 _\///________\///__\////////////_________________________\/////////__\///////////////__\///////////__
#	 A PowerShell Module to streamline common commandline account management related actions, including:
#
#	-"Set-Pass";	a function to set user passwords to either LibertyMMDD, a random alphanumerical string,
#			or a manually selected password. It supports the following:
#			Set-Pass [Username] [-Password-type flag] [password]
#			-Random, will set the password to a random alphanumeric string of 10 to 15 characters
#			-Manual will set the password flag to manual and then look for a password after the '-m' flag
#			if no password follows the '-m', it will prompt for a password.
#	-"Set-Lock";	a function to unlock accounts and to check the status of an account. It supports the following
#			parameters:
#			Set-Lock [Username] [-status]
#			-Status, will not alter the account but instead only return the account's staus('locked' or 'unlocked')	
#			Calling this function without any of the added flags will default to unlocking the account.
#	-"Lockout";	a function to view the lockout status. This function will return whether or not the account is locked,
#			when it was last locked, when it last had a bad password, and the last time the password had been changed
#			it only accepts [Username] as a parameter.
#	-"Get-Groups";	a funtion to view a user's group membership. This function has two parameters:
#			Get-Groups [Username] [-Print]
#			-Print, will export a user's groups to a .csv file located on your desktop titled "$User-Groups.csv"
#	-"Set-Expire";	a function to expire, unexpire, and check on the expiration status of an account. It supports the
#			following parameters:
#			Set-Expire [username] [-Expire] [-Status]
#			-Expire, will expire the account and set the expiration date to the current day at 00:00:00
#			-Status, will not alter the state of the account's expiration status, but will only return a boolean
#			Calling this function without any of the added flags will default to unexpiring the account.
#	-"Get-Info";	a function to "get information" about the account. This function only contains the [username] parameter.
#			It will return the following account variables:
#			-DisplayName, Title, Department, departmentNumber, CN, DistinguishedName, EmailAddress, EmployeeNumber, 
#			Enabled, Created, LastLogonDate, logonCount, OfficePhone, PasswordExpired, PasswordLastSet
#	-"Get-APR";	a function to determine if APR has been setup for the account. This function only contains the [username]
#			parameter. Unlike other functions in this module, Get-APR requires an additional sign in. The prompt will
#			accept either the username or full email.
#	-"Set-Enable";	a function to enable, disable, and determine if an account is enabled or not. This function will only
#			work if the agent has the appropriate permissions (ie T2). Because of this, the Enabled status is also
#			viewable from the `Get-Info` function, meaning that only T2s need to use this specific function. This
#			function supports the following parameters:
#			Set-Enable [username] [-Status] [-Disable]
#			-Status, returns the accounts status in boolean
#			-Disable will disable the account
#			Calling this function without any of the added flags will default to enabling the account.
#	
#
#
#######################
###Utility-Functions###
#######################
function Query-User
{
  return Read-Host 'Username:> '
}

function Get-Cred
{
	if((Test-Path $Home'\Documents\WindowsPowerShell\Modules\Get-APR\user.text') -eq "True")
	{
		$username = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\user.txt'
		$password = Get-Content $Home'\Documents\WindowsPowerShell\Modules\Get-APR\pass.txt' | ConvertTo-SecureString
	}
	else
	{
		$Global:Credential = Get-Credential
		$Global:Username = $credential.username
		$Global:Password = $credential.password
		if(!($credential.username.contains("@")))
		{
			$username = $credential.username + "@liberty.edu"
		}
		else 
		{
			$username = $credential.username
		}

		$password = $credential.password
	}

	$Cred = "" | Select-Object -Property $username, $password
	$Cred.username = $username
	$Cred.password = $password

	return $Cred 
}



########################
###Exported functions###
########################
function Set-Pass #Completed
{
  param(
    [Parameter(Mandatory=$false)]
    [string]$User,

    [Parameter(Mandatory=$false)]
    [switch]$random, #random flag

    [Parameter(Mandatory=$false)]
    [switch]$manual, #manual flag

    [Parameter(Mandatory=$false)]
    [string]$password #Password
  )

  if(!($User)) #Check to see if Username parameter is set
  {
    $User = Query-User
  }

  if($random)
  {
  $auto_pass_flag = 'r'
  }

  if($manual)
  {
  $auto_pass_flag = 'm'
  }

  switch ($auto_pass_flag)
  {
    $null #Default
    {
	$date = Get-Date -Format "MMdd"
	$password = "Liberty${date}"
    }
    'r' #Random
    {
	$asci = [char[]]([char]45..[char]58) + ([char[]]([char]97..[char]122)) + [char[]]([char]65..[char]90)
	$password = (1..$(Get-Random -Minimum 10 -Maximum 15) | % {$asci | get-random}) -join ""
    }
    'm' #Manual
    {
	if(!($password)) #If password was not passed, ask for password
	{
	    $password = Read-Host 'Password:> '
	}
    }
  }
    Set-ADAccountPassword -Identity $User -NewPassword (ConvertTo-SecureString -AsPlainText '$password' -Force)
}

function Set-Lock #Completed
{
  param(
    [Parameter(Mandatory=$false)]
    [string]$User,

    [Parameter(Mandatory=$false)]
    [switch]$status
  )

  if(!($User)) #Check to see if Username parameter is set
  {
    $User = Query-User
  }

  $lock_status = (Get-ADUser $User -properties * | Select-Object LockedOut).LockedOut

  if($status)
  {
    if($lock_status -eq 'True')
    {
      Return "Locked"
    }
    Else
    {
      Return "Unlocked"
    }
  }
  Else
  {
    if($lock_status -eq 'True')
    {
      For ($i=0; $i -le 5; $i++)
      {
	Unlock-ADAccount -Identity $User
      }
    }
  }
}

function LockOut #Completed
{
  param(
    [Parameter(Mandatory=$false)]
    [string]$User
  )

  if(!($User)) #Check to see if Username parameter is set
  {
    $User = Query-User
  }

  Get-ADDomainController -Filter * | ForEach-Object -Process {Get-ADUser -Identity $User -Server $_.Hostname -Properties AccountLockoutTime,LastBadPasswordAttempt,PasswordLastSet,BadPwdCount,LockedOut} | Format-Table -Property LockedOut,AccountLockoutTime,BadPwdCount,LastBadPasswordAttempt,PasswordLastSet -AutoSize
}

function Get-Groups #Completed
{
	param(
		[Parameter(Mandatory=$false)]
		[string]$User,

		[Parameter(Mandatory=$false)]
		[switch]$Print
	)

	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}

	if($print)
	{
		Get-ADPrincipalGroupMembership $User | select name | export-csv -Path "$($env:USERPROFILE)\Desktop\$($User)-Groups.csv"
		Write-Host "Groups exported to $($env:USERPROFILE)\Desktop\$($User)-Groups.csv"
		Return
	}
	else
	{
		Get-ADPrincipalGroupMembership $User | select name
	}
}

function Set-Expire
{
	param(
		
		[Parameter(Mandatory=$false)]
		[string]$User,

		[Parameter(Mandatory=$false)]
		[switch]$Expire,

		[Parameter(Mandatory=$false)]
		[switch]$Status
	)

	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}

	if($Status)
	{
		if(Get-ADUser $User -Properties * | select AccountExpirationDate)
		{
			Return 'True'
		}
		Else
		{
			Return 'False'
		}
	}
	elseif($Expire)
	{
		$date = Get-Date -Format "MM/dd/yyyy"
		Set-ADAccountExpiration -Identity $User -DateTime "$($date) 00:00:00"
		Write-Host "Account has been expired!"
	}
	else
	{
		Clear-ADAccountExpiration -Identity $User
		Write-Host "Account has been unexpired!"
	}
}

function Get-Info #Completed
{
	param(
		[Parameter(Mandatory=$false)]
		[string]$User
	)

	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}
	
	Get-ADUser $User -Properties * | Select DisplayName, Title, Department, departmentNumber, CN, DistinguishedName, EmailAddress, EmployeeNumber, Enabled, Created, LastLogonDate, logonCount, OfficePhone, PasswordExpired, PasswordLastSet

}

function Get-APR #Completed
{
	param(
		[Parameter(Mandatory=$false)]
		[string]$User
	)
	
	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}
	
	if((Test-Path $HOME"\Documents\WindowsPowerShell\Modules\user.txt") -eq "True")
	{
		$username = Get-Content $Home'\Documents\WindowsPowerShell\Modules\user.txt'
		$password = Get-Content $Home'\Documents\WindowsPowerShell\Modules\pass.txt' | ConvertTo-SecureString
	}
	else
	{
		$Global:Credential = Get-Credential
		$Global:Username = $credential.username
		$Global:Password = $credential.password
		if(!($credential.username.contains("@")))
		{
			$username = $credential.username + "@liberty.edu"
		}
		else 
		{
			$username = $credential.username
		}

		$password = $credential.password
	}

	$Cred = New-Object -TypeName System.Management.Automation.PSCredential `
		-ArgumentList $username, $password
		Connect-MsolService -Credential $Cred

	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}

	$UserEmail = $User+'@liberty.edu'
	$Phone = (Get-MsolUser -UserPrincipalName $UserEmail).StrongAuthenticationUserDetails.PhoneNumber
	$Email = (Get-MsolUser -UserPrincipalName $UserEmail).StrongAuthenticationUserDetails.Email

	if (($Phone -eq $null) -and ($Email -eq $null))
	{
		Write-Host "APR is not set."
	}
	else
	{
		Write-Host "APR is set."
	}
}

function Set-Enable
{
	param(
		[Parameter(Mandatory=$false)]
		[string]$User,
		
		[Parameter(Mandatory=$false)]
		[switch]$Status,
		
		[Parameter(Mandatory=$false)]
		[switch]$Disable
	)

	if(!($User)) #Check to see if Username parameter is set
	{
		$User = Query-User
	}

	if($Status)
	{
		$enable_status = Get-ADUser $User -Properties * | Select Enabled
		Return $enable_status
	}
	elseif($Disable)
	{
		Disable-ADAccount -Identity $User	
	}
	else
	{
		Enable-ADAccount -Identity $User
	}
}







#Export-ModuleMember -Function Set-Pass, Set-Lock, LockOut, Get-Groups, Set-Expire, Get-Info, Get-APR, Set-Enable
