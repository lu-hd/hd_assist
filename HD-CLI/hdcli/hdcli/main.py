from config import *
import config

config.globals_()

from hdcli import identity, comment, powershell

today = config.today
notes = config.notes
smarty = config.smart_note
userID = config.userID
r = config.r

def main():
    run = True
    while run == True:
        command = input("$ ")
        #parse(command) #WIP, goal have a parse func. to determine user's request and return said request

        #In the meantime... If/Elif Statements!!!
        if "user" in command:
            clipID = r.clipboard_get()
            clipID = clipID.strip()
            print("Is %s the user?" % clipID)
            prompt = input("(y/n/q): ")
            if prompt == "y":
                identity.set_name(clipID)
                print("User set to %s" % config.userID)
            elif prompt == "n":
                clipID = input("What is the user name?: ")
                identity.set_name(clipID)
                print("User set to %s" % config.userID)
            elif prompt == "q":
                print(" ")
        elif "pass" in command or "pw" in command:
            print("Change the password for %s?" % config.userID)
            prompt = input("(y/n/q): ")
            if prompt == "y":
                powershell.set_password()
                if config.smart_note == True:
                    notes.append("reset pw")
            elif prompt == "n":
                clipID = ("What username would you like to change the password for?: ")
                identity.set_name(clipID)
                powershell.set_password()
                if config.smart_note == True:
                    notes.append("reset pw")
            elif prompt == "q":
                print(" ")
        elif "info" in command:
            powershell.get_info()
        elif "unlock" in command:
            print("Unlock %s's account?" % config.userID)
            prompt = input("(y/n/q): ")
            if prompt == "y":
                powershell.unlock()
                print("%s's account has been unlocked." % config.userID)
                if config.smart_note == True:
                    notes.append("unlocked acct")
            elif prompt == "n":
                clipID = ("What username would you like to unlock?: ")
                identity.set_name(clipID)
                powershell.unlock()
                print("%s's account has been unlocked." % config.userID)
                if config.smart_note == True:
                    notes.append("unlocked acct")
            elif prompt == "q":
                print(" ")
        elif "unexpire" in command:
            print("Unexpire %s's account?" % config.userID)
            prompt = input("(y/n/q): ")
            if prompt == "y":
                powershell.unexpire()
                print("%s's account has been unexpired." % config.userID)
                if config.smart_note == True:
                    notes.append("unexpired acct")
            elif prompt == "n":
                clipID = ("What username would you like to unexpire?: ")
                identity.set_name(clipID)
                powershell.unexpire()
                print("%s's account has been unexpired." % config.userID)
                if config.smart_note == True:
                    notes.append("unexpired acct")
            elif prompt == "q":
                print(" ")
        elif "lockout" in command:
            powershell.lockout()
        elif "group" in command:
            powershell.group()
        elif "apr" in command:
            powershell.APR()

        elif "note" in command:
            comment.add_notes()
        elif "print" in command:
            comment.print_notes()
        elif "sn" in command:
            sn_prompt = input("Turn smart notes on or off?(on/off): ")
            if sn_prompt == "on":
                config.smart_notes = True
            elif sn_prompt == "off":
                config.smart_notes == False


#        elif "ver" in command:

        elif "clear" in command:
            identity.clear_name()
        elif "q" in command:
            run = False
