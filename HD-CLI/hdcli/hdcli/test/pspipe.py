import subprocess, sys
from beautifultable import BeautifulTable

user = "ipringle2"
global p, x, y, z, do, lo, t
def get_info():
    global p, x, z, y, lo, do, t
    p = subprocess.Popen(["powershell.exe", "Get-ADPrincipalGroupMembership '%s' | select name" % (user)], stdout=subprocess.PIPE)
    x = p.communicate()
    y = str(x).split("\\r\\n")
    print(y)
    print("\n".join(y))


def parse(x):
    global y
    y = str(x).split("\\r\\n")
    print("\n".join(y))

def printTable(myDict, colList=None):
   """ Pretty print a list of dictionaries (myDict) as a dynamically sized table.
   If column names (colList) aren't specified, they will show in random order.
   Author: Thierry Husson - Use it as you want but don't blame me.
   """
   if not colList:
       colList = list(myDict.keys() if myDict else [])
   myList = [colList] # 1st row = header
   for item in myDict:
       myList.append([str(item[col] or '') for col in colList])
   colSize = [max(map(len,col)) for col in zip(*myList)]
   formatStr = ' | '.join(["{{:<{}}}".format(i) for i in colSize])
   myList.insert(1, ['-' * i for i in colSize]) # Seperating line
   for item in myList:
       print(formatStr.format(*item))

get_info()
