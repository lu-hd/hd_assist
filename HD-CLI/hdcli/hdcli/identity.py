from config import *
import config

#today = config.today
#notes = config.notes
#smarty = config.smart_note
#userID = config.userID

def set_name(name):
    if verify_name(str(name)) == 1:
        config.userID = str(name)
        #notes = []
        #return userID
    elif verify_name(name) == 2:
        config.userID = get_name(str(name))
        #return userID
        #notes = []

def verify_name(name):
    if re.match("[L|l][0-9]{8}", name):                     #is a LUID
        return 2
    elif re.match("^[a-zA-Z]{2,}[0-9]{0,}", name):          #is a username
        return 1
    else:
        return 0

def get_name(name):
    #searches for username with LUID
    return "username"

def clear_name():
    config.userID = " "
    config.notes = []
    return
