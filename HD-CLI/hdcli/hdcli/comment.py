from config import *
import config

def add_notes():
    go = True
    while go == True:
        prompt = input("Add note: ")
        if prompt == "q":
            go = False
        elif prompt != "q":
            config.notes.append(prompt)

def print_notes():  #WIP
    txt = '\n'.join(config.notes)
    subprocess.run(['clip.exe'], input=txt.strip().encode('utf-16'), check=True)
    print(txt)
