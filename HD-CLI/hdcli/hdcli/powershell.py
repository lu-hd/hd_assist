from config import *
import config

def set_password():
    password = "Liberty%s" % config.today
    subprocess.call(["powershell.exe", "Set-ADAccountPassword -Identity '%s' -NewPassword (ConvertTo-SecureString -AsPlainText '%s' -Force)" % (config.userID, password)])
    print("New password is '%s'" % password)

def get_info():
    p = subprocess.Popen(["powershell.exe", "Get-ADUser '%s' -Properties AccountExpirationDate,DisplayName,CN,DistinguishedName,Department,departmentNumber,Manager,EmailAddress,PasswordLastSet,Created,ipPhone,OfficePhone,msExchRecipientTypeDetails,LastLogonDate" % (config.userID)], stdout=subprocess.PIPE)
    x = p.communicate()
    parse(x, 2, 4)

def lockout():
    p = subprocess.Popen(["powershell.exe", "Get-ADDomainController -Filter * | ForEach-Object -Process {Get-ADUser -Identity '%s' -Server $_.Hostname -Properties AccountLockoutTime,LastBadPasswordAttempt,PasswordLastSet,BadPwdCount,LockedOut} | select LockedOut,AccountLockoutTime,BadPwdCount,LastBadPasswordAttempt,PasswordLastSet" % (config.userID)], stdout=subprocess.PIPE)
    x = p.communicate()
    y = str(x).split("\\r\\n")

    #Searches through text that PS Command returns and pulls out the relevant strings.
    z = []
    z.append([s for s in y if "LockedOut" in s])
    z.append([s for s in y if "AccountLockoutTime" in s])
    z.append([s for s in y if "BadPwdCount" in s])
    z.append([s for s in y if "LastBadPasswordAttempt" in s])
    z.append([s for s in y if "PasswordLastSet" in s])

    tlo = BeautifulTable()
    lo =[["Locked Out"], ["Louckout Time"], ["Bad PW Count"], ["Last Bad PW"], ["PW Last Set"]]
    tlo.column_headers = ["Domain Controller", "D03 - 10.255.80.18", "D04 - 10.255.80.19", "D05 - 10.255.80.20", "D06 - 10.255.80.21"]

    #Extracts relevant data from each extracted string and adds them to list `lo`, and then adds to table `tlo`.
    i = 0
    for i in range(len(z)):
        j = 0
        for j in range(len(z[0])):
            lo[i].append(z[i][j][z[i][j].find(":")+2:])
    i = 1
    for i in range(5):
        tlo.append_row(lo[i])

    print(tlo)

def unexpire():
    subprocess.call(["powershell.exe", "Clear-ADAccountExpiration -Identity '%s'" % (config.userID)])

def unlock():
    p = subprocess.Popen(["powershell.exe", "Unlock-ADAccount -Identity '%s'" % (config.userID)], stdout=subprocess.PIPE)
    x = p.communicate()


def group():
    p = subprocess.Popen(["powershell.exe", "Get-ADPrincipalGroupMembership '%s' | select name" % (config.userID)], stdout=subprocess.PIPE)
    x = p.communicate()
    parse(x, 3, 3)

def APR():
    subprocess.call(["powershell.exe", "Get-APR -User '%s'" % (config.userID)])

def parse(x, a, b): #Tuple to be parsed, the first item is the subsequent list to be printed, and the last string to be printed
    y = str(x).split("\\r\\n")
    print("\n".join(y[a:-b]))
