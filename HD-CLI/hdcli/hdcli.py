#from config import *
import hdcli.main

print("""
HelpDesk CLI (Command Line Interface) Version 0.1.0
Author: Ian S. Pringle
Email: ipringle2@liberty.edu

Copyright (C) 2018 Ian S. Pringle
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO warranty.

This software was intended for the use of Liberty University's Helpdesk only.
""")

hdcli.main.main()
