import re
import subprocess
import time
import os
from datetime import date
from tkinter import Tk



#To call a Powershell command use "subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "[POWERSHELL COMMAND HERE]"])"

global userID
global today
global r
global smart_note

notes = []

r = Tk()

smart_note = False

d = date.today()
today = d.strftime("%m%d")

def verify_name(name):
    if re.match("[L|l][0-9]{8}", name):                     #is a LUID
        return 2
    elif re.match("^[a-zA-Z]{2,}[0-9]{0,}", name):          #is a username
        return 1
    else:
        return 0

def get_name(name):
    #searches for username with LUID
    return "username"

def set_name(name):
    global userID, notes
    if verify_name(str(name)) == 1:
        userID = str(name)
        #notes = []
        return
    elif verify_name(name) == 2:
        userID = get_name(str(name))
        #notes = []

def clear_name():
    userID = NULL
    return

def set_password():
    password = "Liberty%s" % today
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Set-ADAccountPassword -Identity '%s' -NewPassword (ConvertTo-SecureString -AsPlainText '%s' -Force)" % (userID, password)])
    print("New password is '%s'" % password)

def get_info():
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Get-ADUser '%s' -Properties AccountExpirationDate,DisplayName,CN,DistinguishedName,Department,departmentNumber,Manager,EmailAddress,PasswordLastSet,Created,ipPhone,OfficePhone,msExchRecipientTypeDetails" % (userID)])

def lockout():  #WIP
   subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Get-ADDomainController -Filter * | ForEach-Object -Process {Get-ADUser -Identity '%s' -Server $_.Hostname -Properties AccountLockoutTime,LastBadPasswordAttempt,BadPwdCount,LockedOut} | select AccountLockoutTime,BadPwdCount,Enabled,LastBadPasswordAttempt,LockedOut" % (userID)])

def unexpire():
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Clear-ADAccountExpiration -Identity '%s'" % (userID)])

def unlock():
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Unlock-ADAccount -Identity '%s'" % (userID)])

def group():
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Get-ADPrincipalGroupMembership '%s' | select name" % (userID)])

def APR():
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Get-APR -User '%s'" % (userID)])

def note():
    global notes
    go = True
    while go == True:
        prompt = input("Add note: ")
        if prompt == "q":
            go = False
        elif prompt != "q":
            notes.append(prompt)

def print_notes():  #WIP
    global notes    #Mostly b0rked. Can only print to terminal atm, and even then only strings, not ints.
    txt = '\n'.join(notes)
    subprocess.run(['clip.exe'], input=txt.strip().encode('utf-16'), check=True)
    print(txt)


run = True

print("""
                Helper CLI (Command Line Interface) Version 0.1.0
                Author: Ian S. Pringle
                Email: ipringle2@liberty.edu

                Copyright (C) 2018 Ian S. Pringle
                License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
                This is free software: you are free to change and redistribute it.
                There is NO warranty.

                This software was intended for the use of Liberty University's Helpdesk only.
    """)

while run == True:

    command = input(">_: ")
    if "user" in command:
        userID = r.clipboard_get()
        userID = userID.strip()
        print("Is %s the user?" % userID)
        prompt = input("(y/n/q): ")
        if prompt == "y":
            set_name(userID)
            print("User set to %s" % userID)
        elif prompt == "n":
            userID = input("What is the user name?: ")
            set_name(userID)
            print("User set to %s" % userID)
            if smart_note == True:
                notes.append("unlocked acct")
        elif prompt == "q":
            print(" ")
    elif "pass" in command or "pw" in command:
        print("Change the password for %s?" % userID)
        prompt = input("(y/n/q): ")
        if prompt == "y":
            set_password()
            if smart_note == True:
                notes.append("unlocked acct")
        elif prompt == "n":
            userID = ("What username would you like to change the password for?: ")
            set_password()
            if smart_note == True:
                notes.append("reset pw")
        elif prompt == "q":
            print(" ")
    elif "info" in command:
        get_info()
    elif "unlock" in command:
        print("Unlock %s's account?" % userID)
        prompt = input("(y/n/q): ")
        if prompt == "y":
            unlock()
            print("%s's account has been unlocked." % userID)
            if smart_note == True:
                notes.append("unlocked acct")
        elif prompt == "n":
            userID = ("What username would you like to unlock?: ")
            unlock()
            print("%s's account has been unlocked." % userID)
            if smart_note == True:
                notes.append("unlocked acct")
        elif prompt == "q":
            print(" ")
    elif "unexpire" in command:
        print("Unexpire %s's account?" % userID)
        prompt = input("(y/n/q): ")
        if prompt == "y":
            unexpire()
            print("%s's account has been unexpired." % userID)
            if smart_note == True:
                notes.append("unexpired acct")
        elif prompt == "n":
            userID = ("What username would you like to unexpire?: ")
            unexpire()
            print("%s's account has been unexpired." % userID)
            if smart_note == True:
                notes.append("unexpired acct")
        elif prompt == "q":
            print(" ")
    elif "lockout" in command:
        lockout()
    elif "group" in command:
        group()
    elif "apr" in command:
        APR()

    elif "note" in command:
        note()
    elif "print" in command:
        print_notes()
    elif "smart note" in command:
        sn_prompt = input("Turn smart notes on or off?(on/off): ")
        if sn_prompt == "on":
            smart_notes = True
        elif sn_prompt == "off":
            smart_notes == False

    elif "clear" in command:
        userID = ""
        notes = []
    elif "q" in command:
        run = False
