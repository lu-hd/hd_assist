import re
import subprocess
import time
import os
from datetime import date
from tkinter import Tk
import sys
from beautifultable import BeautifulTable

def globals_():
    global userID, today, r, notes, smart_note
    notes = []
    smart_note = False

    userID = ""

    r = Tk()
    d = date.today()
    today = d.strftime("%m%d")
