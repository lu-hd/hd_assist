#Need to install requests package for python
#easy_install requests
import requests
import json

# Set the request parameters
url = 'https://libertydev.service-now.com/api/now/table/new_call?sysparam_fields=number'

# Eg. User name="admin", Password="admin" for this code sample.
user = 'username'
pwd = 'password'
# Set proper headers
headers = {"Content-Type":"application/json","Accept":"application/json"}

#Set the dictionary for the POST request
req = {
"u_phone_number":"8008888888",
"cmdb_ci":"Blackboard",
"short_description":"User cannot submit assignment",
"work_notes":"Use was on safari. Advised to use FF or Chrome. NFQ",
"assigned_to":"ipringle2",
"u_firstcall_resolution":"true",
"time_worked":"00:02:00",
"u_caller":"Ian Pringle",
"u_username":"ipringle2",
"u_id_number":"L00000000"
        }

# Do the HTTP request
response = requests.post(url, auth=(user, pwd), headers=headers ,data=json.dumps(req))

# Check for HTTP codes other than 200
if response.status_code != 200: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()
print(data)
