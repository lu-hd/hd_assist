import cred
import requests

def get_user(luid):
	# Set the request parameters
	url = f'https://libertydev.service-now.com/api/now/table/sys_user?sysparm_query=employee_number%3D{luid}&sysparm_fields=user_name'
	
	# Eg. User name="admin", Password="admin" for this code sample.
	usr = cred.usr
	pwd = cred.pwd
	
	# Set proper headers
	headers = {"Content-Type":"application/json","Accept":"application/json"}
	
	# Do the HTTP request
	response = requests.get(url, auth=(usr, pwd), headers=headers )

	# Check for HTTP codes other than 200
	if response.status_code != 200:
		print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
		exit()
	
	# Decode the JSON response into a dictionary and use the data
	data = response.json()
	return data['result'][0]['user_name'] 
	# Set applicable data

luids = [line.rstrip('\n') for line in open('luids.txt')]
usernames = []

for luid in luids:
	usernames.append(get_user(luid))

with open('usernames.txt') as file:
	for user in usernames:
		file.write("%s\n" % user)
