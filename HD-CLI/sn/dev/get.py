#Need to install requests package for python
#easy_install requests
import requests
import cred
import json

# Set the request parameters
url = 'https://libertydev.service-now.com/api/now/table/new_call?sysparm_query=number%3DCALL0937138'
#url = 'https://libertydev.service-now.com/api/now/table/cmn_department?sysparam_fields=sys_id%2Cname'
#url = 'https://libertydev.service-now.com/api/now/table/new_call/0beddaaf13691b00a612b2776144b09e'
#url = 'https://libertydev.service-now.com/api/now/table/sys_template?sysparm_query=table%3Dnew_call^active%3Dtrue^group%3D81545a7c7b5b4000b9a93a5c8f4d4d88&sysparm_fields=name%2Csys_id%2Ctemplate'

# Eg. User name="admin", Password="admin" for this code sample.
user = cred.usr
pwd = cred.pwd

# Set proper headers
headers = {"Content-Type":"application/json","Accept":"application/json"}

# Do the HTTP request
response = requests.get(url, auth=(user, pwd), headers=headers )

# Check for HTTP codes other than 200
if response.status_code != 200: 
	print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
	exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()
print(json.dumps(data, indent=4, sort_keys=True))


#file = open('transfer_external.py','w')
#file.write(json.dumps(data,indent=4, sort_keys=True))
#file.close()
