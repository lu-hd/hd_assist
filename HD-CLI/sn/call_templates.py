templates = {    
		"IPTV":
		{
            "name": "IPTV",
            "sys_id": "029efa7d2b394a004d7f717bf8da1518",
            "short_description": "IPTV",
			"cmdb_ci": "2ed7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
		"Wireless Network":
		{
            "name": "Wireless Network",
            "sys_id": "033fa2352bf54a004d7f717bf8da15a2",
            "short_description": "Wireless Network",
			"cmdb_ci": "7ec0bed74c2d30045af1e94cc54a16b7",
			"comments": "",
        },
		"Blitz Questions":
		{
            "name": "Blitz Questions",
            "sys_id": "03e110d22b71c60082f289efe8da154c",
            "cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"short_description": "Blitz Questions",
			"comments": "",
        },
		"Adobe ID Rollout (Student)":
		{
            "name": "Adobe ID Rollout (Student)",
            "sys_id": "0450e4da2bb1c60082f289efe8da157b",
            "short_description": "Adobe ID Rollout (Student)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
		"Bb - Exam":
		{
            "name": "Bb - Exam",
            "sys_id": "0477b2b12b394a004d7f717bf8da155c",
            "short_description": "Blackboard - Exam",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
		"3rdP Bb -  (Pearson - Non MyLab)":
		{
            "name": "3rdP Bb -  (Pearson - Non MyLab)",
            "sys_id": "0ab03d0e2b4e46004d7f717bf8da152d",
            "short_description": "Blackboard - 3rd Party Integration (Pearson - Non MyLab)",
			"comments": "",
			"cmdb_ci": "5f3b103835c0a5405af1cb6de5727fd1",
        },
        "Account Access (Automated PW Reset)":
		{
            "name": "Account Access (Automated PW Reset)",
            "sys_id": "0fe837ab137e72c03619b2776144b087",
            "cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Account Access (Automated Password Reset)",
			"comments": "The steps and links listed in the following article will allow you to set-up Automated Password Reset so that you can reset your password through a verified phone number or alternate email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nAfter you have completed this one-time registration, you will now be able to quickly and securely reset your own password at your convenience. \r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"u_kb": "true",
			"u_call_type": "Password Reset",
        },
        "Canvas / Instructure / Thinkwell":
		{
            "name": "Canvas / Instructure / Thinkwell",
            "sys_id": "1524897b2bb9420082f289efe8da15ad",
            "short_description": "Canvas",
			"cmdb_ci": "8572d49ae0e551045af14824d000d8b3",
			"comments": "",
        },
        "Media/AV Systems":
		{
            "name": "Media/AV Systems",
            "sys_id": "16cbcbeb6f5f46804508e90d2c3ee479",
            "short_description": "Media/AV Systems",
			"cmdb_ci": "2ad7b05c2ce31044b9a9c4f53e832aea",
			"comments": "",
        },
        "Fax / StoneFax Issues":
		{
            "name": "Fax / StoneFax Issues",
            "sys_id": "1b6fd52b2b7d02004d7f717bf8da1501",
            "short_description": "Fax / StoneFax Issues",
			"cmdb_ci": "38686428e0451d405af14824d000d80a",
			"comments": "",
        },
        "3rd Party Website":
		{
            "name": "3rd Party Website",
            "sys_id": "1c3cb93013ee1a003619b2776144b04b",
            "short_description": "3rd Party Website (WEBSITE)",
			"cmdb_ci": "33667d1f0a0a3c9e01a6addbef08cbee",
			"comments": "",
        },
        "Account Access (MyLU Issues)":
		{
            "name": "Account Access (MyLU Issues)",
            "sys_id": "21981feb13bbd2843619b2776144b051",
            "short_description": "Account Access (MyLU Issues)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
        },
        "External - Freedom Aviation":
		{
            "name": "External - Freedom Aviation",
            "sys_id": "219abaf52b394a004d7f717bf8da1594",
            "short_description": "Freedom Aviation (ISSUE)",
			"comments": "",
        },
        "3rdP Bb -  (Pearson MyDevelopmentLab)":
		{
            "name": "3rdP Bb -  (Pearson MyDevelopmentLab)",
            "sys_id": "2386f1c62b8e46004d7f717bf8da151d",
            "short_description": "Blackboard - 3rd Party Integration (Pearson - MyDevelopmentLab)",
			"comments": "",
			"cmdb_ci": "2f8ce768f5349900f47c48c0f722fa6a",
        },
        "Chat - Username/Password Decline":
		{
            "name": "Chat - Username/Password Decline",
            "sys_id": "247b22482b06c20082f289efe8da15e1",
            "short_description": "Chat - Username/Password Decline",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
			"u_queue": "Chat",
        },
        "Bb - Discussion Board":
		{
            "name": "Bb - Discussion Board",
            "sys_id": "25a5fabd2bf54a004d7f717bf8da153e",
            "short_description": "Blackboard - Discussion Board",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "3rd Party Website (tutor.com)":
		{
            "name": "3rd Party Website (tutor.com)",
            "sys_id": "2626c1a0dbf45f040dcf56b8dc961951",
			"short_description": "3rd Party Website (tutor.com)",
        },
        "Asset Recovery":
		{
            "name": "Asset Recovery",
            "sys_id": "276f4ab313ed3200935b7e076144b06a",
            "short_description": "Asset Recovery (COMPUTERNAME)",
			"cmdb_ci": "f465f61b9c48ad40f47c19537d850c82",
        },
        "Call Handling - IPCOMM Failure":
		{
            "name": "Call Handling - IPCOMM Failure",
            "sys_id": "297e247c131947841c19dc228144b08d",
            "short_description": "Call Handling - HDRS IPCOMM Failure",
			"cmdb_ci": "8ed7b05c2ce31044b9a9c4f53e832ae9",
        },
        "Lync (Sign-In)":
		{
            "name": "Lync (Sign-In)",
            "sys_id": "2b347a3d2bf54a004d7f717bf8da15af",
            "short_description": "Lync (Sign-In)",
			"cmdb_ci": "ead7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Banner Access and Oracle PW":
		{
            "name": "Banner Access and Oracle PW",
            "sys_id": "2b43264213167e40edaf76076144b093",
            "short_description": "Issue accessing Banner and/or prompted for Oracle Password",
			"cmdb_ci": "aad7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Skype For Business (Sign-In)":
		{
            "name": "Skype For Business (Sign-In)",
            "sys_id": "2b74727d2bf54a004d7f717bf8da1500",
            "short_description": "Skype For Business (Sign-In)",
			"cmdb_ci": "ead7b05c2ce31044b9a9c4f53e832aeb",
			"u_firstcall_resolution": "true",
			"comments": "",
        },
        "Banner INB":
		{
            "name": "Banner INB",
            "sys_id": "2c482ea6133e1a401c19dc228144b0d3",
            "short_description": "Banner INB",
			"cmdb_ci": "aad7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Videos - Website/Flames":
		{
            "name": "Videos - Website/Flames",
            "sys_id": "2e4388ac6f1bca404508e90d2c3ee410",
            "short_description": "Videos - Website/Flames",
			"cmdb_ci": "2ad7b05c2ce31044b9a9c4f53e832aed",
			"comments": "",
        },
        "Permissions Issue":
		{
            "name": "Permissions Issue",
            "sys_id": "2e8abaf52b394a004d7f717bf8da151d",
            "short_description": "Permissions Issue (PERMISSIONTYPE)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
        },
        "Mobile Device":
		{
            "name": "Mobile Device",
            "sys_id": "34db7e792b394a004d7f717bf8da150c",
            "short_description": "Mobile Device Help (ISSUE)",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "3rdP Bb - (Cengage)":
		{
            "name": "3rdP Bb - (Cengage)",
            "sys_id": "37467d862b8e46004d7f717bf8da1510",
            "short_description": "Blackboard - 3rd Party Integration (Cengage)",
			"comments": "",
			"cmdb_ci": "a0e9865cf5781900f47c48c0f722faad",
        },
        "Online Library":
		{
            "name": "Online Library",
            "sys_id": "3aaa7ef52b394a004d7f717bf8da15c8",
            "short_description": "Online Library",
			"cmdb_ci": "c94e250868e539049551c8db769d4f27",
			"comments": "",
        },
        "3rdP Bb -  (Pearson MyPsychLab)":
		{
            "name": "3rdP Bb -  (Pearson MyPsychLab)",
            "sys_id": "4186b1c62b8e46004d7f717bf8da15fe",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyPsychLab)",
			"comments": "",
			"cmdb_ci": "c6c01940f5ea1108f47c48c0f722fa22",
        },
        "EZProxy":
		{
            "name": "EZProxy",
            "sys_id": "4568fef12b394a004d7f717bf8da1597",
            "short_description": "EZProxy",
			"cmdb_ci": "66d7b05c2ce31044b9a9c4f53e832aee",
			"comments": "",
        },
        "Bb - Faculty Assistance":
		{
            "name": "Bb - Faculty Assistance",
            "sys_id": "4977f2b12b394a004d7f717bf8da158d",
            "short_description": "Blackboard - Faculty Assistance (<ISSUE>)",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "3rdP Bb -  (LiveText)":
		{
            "name": "3rdP Bb -  (LiveText)",
            "sys_id": "4a4351e62bb10e004d7f717bf8da1590",
            "short_description": "Blackboard - 3rd Party Integration (LiveText)",
			"comments": "",
			"cmdb_ci": "024ff978681f65005af19b008fde200b",
        },
        "Divinity Questionnaire":
		{
            "name": "Divinity Questionnaire",
            "sys_id": "4aaaf6f52b394a004d7f717bf8da15d5",
            "short_description": "Divinity Questionnaire",
			"cmdb_ci": "52d7b05c2ce31044b9a9c4f53e832aea",
			"comments": "",
        },
        "Civitas":
		{
            "name": "Civitas",
            "sys_id": "4aaf2b786fef0a804508e90d2c3ee444",
            "short_description": "Civitas",
			"cmdb_ci": "f4407c732b39420082f289efe8da15bc",
			"comments": "",
        },
        "Bb - SafeAssign":
		{
            "name": "Bb - SafeAssign",
            "sys_id": "4b7776b12b394a004d7f717bf8da158b",
            "short_description": "Blackboard - SafeAssign",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Laptop - Personal":
		{
            "name": "Laptop - Personal",
            "sys_id": "4e0db6f92b394a004d7f717bf8da150d",
            "short_description": "Personal Laptop",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "IT Marketplace (Access)":
		{
            "name": "IT Marketplace (Access)",
            "sys_id": "501683392b794a004d7f717bf8da155e",
            "short_description": "IT Marketplace (Access)",
			"cmdb_ci": "4846b65b9c48ad40f47c19537d850c22",
			"comments": "",
        },
        "Argos - Report/Server Issues":
		{
            "name": "Argos - Report/Server Issues",
            "sys_id": "504388ac6f1bca404508e90d2c3ee465",
            "short_description": "Argos - Report/Server Issues",
			"cmdb_ci": "33667bcc0a0a3c9e005720ae8b742ab1",
			"comments": "",
        },
        "Application for Liberty (VZCollegeApp)":
		{
            "name": "Application for Liberty (VZCollegeApp)",
            "sys_id": "504d92ee131a52001c19dc228144b007",
            "short_description": "Application for Liberty (VZCollegeApp)",
			"cmdb_ci": "272b0cb5386c30007046847eea261b76",
			"comments": "",
        },
        "WebDT":
		{
            "name": "WebDT",
            "sys_id": "51274f792b794a004d7f717bf8da15be",
            "short_description": "WebDT Box",
			"cmdb_ci": "eed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Account Access (Claim Account)":
		{
            "name": "Account Access (Claim Account)",
            "sys_id": "55edae712bf54a004d7f717bf8da15b1",
            "short_description": "Account Access (Claim Account)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"u_call_type": "Question",
        },
        "DCP Audit":
		{
            "name": "DCP Audit",
            "sys_id": "5b2ba5982b75020082f289efe8da15ac",
            "short_description": "DCP Audit",
			"cmdb_ci": "e6d7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "MyLU":
		{
            "name": "MyLU",
            "sys_id": "5bf15bf06f6f0a804508e90d2c3ee438",
            "short_description": "MyLU Issues",
			"cmdb_ci": "e32b0cb5386c30007046847eea261b7b",
			"comments": "",
        },
        "Desktop - Personal":
		{
            "name": "Desktop - Personal",
            "sys_id": "5c1db6f92b394a004d7f717bf8da1510",
            "short_description": "Personal Desktop",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "3rdP Bb -  (Peregrine Academics)":
		{
            "name": "3rdP Bb -  (Peregrine Academics)",
            "sys_id": "5e25073c6fff06c04508e90d2c3ee451",
            "short_description": "Blackboard - 3rd Party Integration (Peregrine Academics)",
			"comments": "",
			"cmdb_ci": "214d074535e429805af1cb6de5727f12",
        },
        "Adobe ID Rollout (Faculty/Staff)":
		{
            "name": "Adobe ID Rollout (Faculty/Staff)",
            "sys_id": "610060da2bb1c60082f289efe8da155f",
            "short_description": "Adobe ID Rollout (Faculty/Staff)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Email - Office 365":
		{
            "name": "Email - Office 365",
            "sys_id": "61ad6958137d2e80edaf56076144b0cc",
            "short_description": "Email - Office 365",
			"cmdb_ci": "fcfe3204a894e480b9a994d13108acc4",
			"comments": "",
        },
        "VPN":
		{
            "name": "VPN",
            "sys_id": "61db7e792b394a004d7f717bf8da15d2",
            "short_description": "VPN (VPN Box or Anyconnect Client)",
			"cmdb_ci": "ba5fb34f64bf49005af169f0f23e87f4",
			"comments": "",
        },
        "Account Access (Change Password)":
		{
            "name": "Account Access (Change Password)",
            "sys_id": "6c0c32ca2bf5860082f289efe8da1506",
            "cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Account Access (Change Password)",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"call_type": "Password Reset",
        },
        "Wired Network":
		{
            "name": "Wired Network",
            "sys_id": "6ddadd29134b5a803619b2776144b048",
            "short_description": "Wired Network",
			"cmdb_ci": "aad7b05c2ce31044b9a9c4f53e832aed",
			"comments": "",
        },
        "Bb - New Box":
		{
            "name": "Bb - New Box",
            "sys_id": "6e566745138ada001c19dc228144b02f",
            "short_description": "Blackboard - New Box",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Call Handling - No Answer":
		{
            "name": "Call Handling - No Answer",
            "sys_id": "6e7836352b394a004d7f717bf8da156d",
            "short_description": "No Answer",
			"cmdb_ci": "8ed7b05c2ce31044b9a9c4f53e832ae9",
			"comments": "",
        },
        "3rdP Bb -  (Pearson MyMathLab)":
		{
            "name": "3rdP Bb -  (Pearson MyMathLab)",
            "sys_id": "6e867d862b8e46004d7f717bf8da1564",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyMathLab)",
			"comments": "",
			"cmdb_ci": "a2b9465cf5781900f47c48c0f722fa94",
        },
        "Email - Scam/Phishing Message":
		{
            "name": "Email - Scam/Phishing Message",
            "sys_id": "6fac0ae09d1b89c0f47ceada3b506056",
            "cmdb_ci": "62d7b05c2ce31044b9a9c4f53e832aed",
			"short_description": "Email - Scam/Phishing Message",
			"comments": "Knowledge article KB0011021:\r\n[code]<h2>How can I stay safe against phishing and email scams?</h2>\r\n<p>An<span style=\"font-weight: bold;\"> Email Scam</span> is an email with the deliberate intent to defraud you or trick you into giving away money or possessions.</p>\r\n<p>A<span style=\"font-weight: bold;\"> Phishing</span> <span style=\"font-weight: bold;\">Attempt </span>is an email that appears to be from an official source or a pop-up message designed to&nbsp;trick unsuspecting users into sharing private information and/or password and account information.</p>\r\n<p>Stay safe from scams and phishing attacks by following these suggestions:</p>\r\n<ul>\r\n<li><span style=\"font-weight: bold;\">Never</span> reply to a message that asks for your username and password. No legitimate organization, including Liberty University, should ever ask you for your password or other sensitive information via email.</li>\r\n<li><span style=\"font-weight: bold;\">Don't</span> send personal information, such as Social Security numbers and credit card numbers via email. Email is as insecure as a postcard; it can be read any point along the way to its final destination.</li>\r\n<li><span style=\"font-weight: bold;\">Don't</span> click on links in phishing messages. Scam artists can&nbsp;create fake login pages that look legitimate and will&nbsp;steal&nbsp;your log-in information.</li>\r\n<li>Report any phishing messages you receive in your Liberty email account to <strong>s</strong><strong>cams@liberty.edu </strong></li>\r\n<li>Immediately change your password if you feel that you may have accidentally replied to a phishing message.</li>\r\n</ul>\r\n<h3>How to block a spam email account in Outlook:</h3>\r\n<p style=\"font-weight: bold;\"><img style=\"align: bottom;\" src=\"/sys_attachment.do?sys_id=f0de08176f4c92004508e90d2c3ee463\" alt=\"\" width=\"650\" height=\"132\" align=\"bottom\" /></p>\r\n<p>If you receive a spam message, on the Home tab (or the Message tab), select the <strong>Junk</strong> option, and then click <strong>Block Sender</strong> in the dropdown menu.</p>\r\n<p style=\"font-weight: bold;\"><img style=\"align: baseline;\" title=\"\" src=\"/kb0011021 junk.PNGx\" alt=\"\" align=\"bottom\" border=\"\" hspace=\"\" vspace=\"\" /></p>\r\n<p style=\"font-weight: bold;\">&nbsp;</p>\r\n<p>If you receive a notification that a message is in quarantine, please visit: <a title=\"https://admin.protection.outlook.com/Quarantine/?wa=wsignin1.0\" href=\"https://admin.protection.outlook.com/Quarantine/?wa=wsignin1.0\" target=\"_blank\">https://admin.protection.outlook.com/Quarantine/?wa=wsignin1.0</a></p>\r\n<p>Once signed in with your Liberty credentials, you can review the messages and select the ones you wish to release.</p>\r\n<p style=\"font-weight: bold;\"><img style=\"align: bottom;\" src=\"/sys_attachment.do?sys_id=fda511c4139a56401c19dc228144b0f6\" alt=\"\" width=\"650\" height=\"291\" align=\"bottom\" /></p>\r\n<p>This <a title=\"link\" href=\"https://support.office.com/en-us/article/Block-a-mail-sender-b29fd867-cac9-40d8-aed1-659e06a706e4\" target=\"_blank\">link</a> will provide instructions on how to edit/block/set a sender. As always, you may contact the Liberty University IT HelpDesk at 866-447-2869 or <a title=\"Chat with us\" href=\"http://www.liberty.edu/index.cfm?PID=20878\" target=\"_blank\">Chat with us</a> for further assistance.</p>[/code]",
        },
        "Call Handling - Hang Up":
		{
            "name": "Call Handling - Hang Up",
            "sys_id": "7078b2352b394a004d7f717bf8da15b4",
            "short_description": "Hang Up",
			"cmdb_ci": "8ed7b05c2ce31044b9a9c4f53e832ae9",
			"comments": "",
        },
        "3rdP Bb -  (Pearson MyEducationLab)":
		{
            "name": "3rdP Bb -  (Pearson MyEducationLab)",
            "sys_id": "7697fd0a2b8e46004d7f717bf8da1572",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyEducationLab)",
			"comments": "",
			"cmdb_ci": "161bdcf435c0a5405af1cb6de5727f9b",
        },
        "Office 2016 Migration - PC":
		{
            "name": "Office 2016 Migration - PC",
            "sys_id": "785504ae135ffe481c19dc228144b0da",
            "u_call_type": "Question",
			"short_description": "Office 2016 Migration Issues - PC",
			"parent": "ec3244f3137e72c01c19dc228144b06e",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
        },
        "ProDev Portal":
		{
            "name": "ProDev Portal",
            "sys_id": "78f77c37132dbe80edaf76076144b0ab",
            "short_description": "Professional Development Portal",
			"cmdb_ci": "992276579c48ad40f47c19537d850c51",
        },
        "Page Load Issues - Cache/Cookies":
		{
            "name": "Page Load Issues - Cache/Cookies",
            "sys_id": "794887e76f5f46804508e90d2c3ee413",
            "comments": "",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"short_description": "Page Load Issues - Cache/Cookies",
        },
        "Bb - EBook Access":
		{
            "name": "Bb - EBook Access",
            "sys_id": "7b75a1f42b0e860082f289efe8da1580",
            "short_description": "Blackboard - EBook Access",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Kaltura":
		{
            "name": "Kaltura",
            "sys_id": "7b918f7713dccfc43619b2776144b0b4",
            "time_worked": "javascript:gs.getDurationDate('0 0:0:0')",
			"u_call_type": "Question",
			"u_complaint": "false",
			"cmdb_ci": "9c62a4be13ed2a80935b7e076144b06f",
			"short_description": "Kaltura",
        },
        "Office 365 (Web)":
		{
            "name": "Office 365 (Web)",
            "sys_id": "7bd3323d2bf54a004d7f717bf8da15f0",
            "short_description": "Office 365 (Web)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Bb - Course Restore Request":
		{
            "name": "Bb - Course Restore Request",
            "sys_id": "7dc572fd2bf54a004d7f717bf8da1590",
            "short_description": "Blackboard - Course Restore Request",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Laptop - LU":
		{
            "name": "Laptop - LU",
            "sys_id": "7efcb6f92b394a004d7f717bf8da151d",
            "short_description": "LU Laptop (COMPUTERNAME)",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "Printer":
		{
            "name": "Printer",
            "sys_id": "7fcb7e792b394a004d7f717bf8da151c",
            "short_description": "Printer Help",
			"cmdb_ci": "8b23b0fc015a49005af1fad06158f25b",
			"comments": "",
        },
        "Bb - Other":
		{
            "name": "Bb - Other",
            "sys_id": "818ec821135716c01c19dc228144b0f8",
            "short_description": "Blackboard - Other Blackboard Unresponsive/ Outage",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "3rdP Bb -  (Zondervan/SCORM)":
		{
            "name": "3rdP Bb -  (Zondervan/SCORM)",
            "sys_id": "81e86db82be6ce404d7f717bf8da15ef",
            "short_description": "Blackboard - 3rd Party Integration (Zondervan/SCORM)",
			"comments": "",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
        },
        "WebEx":
		{
            "name": "WebEx",
            "sys_id": "82fefe7d2b394a004d7f717bf8da156b",
            "short_description": "WebEx",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Faculty Portfolio":
		{
            "name": "Faculty Portfolio",
            "sys_id": "8624bdfa13a95a001c19dc228144b035",
            "short_description": "Faculty Portfolio",
			"cmdb_ci": "3f5d6ed99c639500f47c19537d850cb2",
			"comments": "",
        },
        "Bb - Account Disabled":
		{
            "name": "Bb - Account Disabled",
            "sys_id": "8a7776b12b394a004d7f717bf8da157d",
            "short_description": "Blackboard - Account Disabled",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Skype For Business (Install/Use)":
		{
            "name": "Skype For Business (Install/Use)",
            "sys_id": "8ca47e3d2bf54a004d7f717bf8da154e",
            "short_description": "Skype For Business (Install/Use)",
			"cmdb_ci": "ead7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Account Access (Self-Report Compromised)":
		{
            "name": "Account Access (Self-Report Compromised)",
            "sys_id": "8cbea2f12bf54a004d7f717bf8da1550",
            "short_description": "Account Access (Self-Report Compromised)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"u_call_type": "Password Reset",
        },
        "3rdP Bb -  (Pearson MyHistoryLab)":
		{
            "name": "3rdP Bb -  (Pearson MyHistoryLab)",
            "sys_id": "8d96f1c62b8e46004d7f717bf8da151e",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyHistoryLab)",
			"comments": "",
			"cmdb_ci": "6a521dc0f5ea1108f47c48c0f722fa5e",
        },
        "Call List Removal":
		{
            "name": "Call List Removal",
            "sys_id": "8ef292b92b354a004d7f717bf8da151e",
            "cmdb_ci": "62d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Call List Removal",
			"u_transfer": "true",
			"u_transfer_list": "aadcb3ed79c049045af1d2eb7ec6bbba",
			"comments": "Person calling would like to be removed from the call list KB0012629\r\n\r\nTransferred user to 555-4401 for removal",
        },
        "Dropbox":
		{
            "name": "Dropbox",
            "sys_id": "902be62b137ce6043619b2776144b0a7",
            "short_description": "Dropbox",
			"comments": "",
			"cmdb_ci": "0619da042b6b710082f289efe8da1592",
        },
        "Chat - No Answer":
		{
            "name": "Chat - No Answer",
            "sys_id": "90fff1622b3d0a0082f289efe8da151e",
            "short_description": "Chat - No Answer",
			"cmdb_ci": "547e7938ec93a500f47cd2fef70934fd",
			"comments": "",
			"u_queue": "Chat",
        },
        "Chat - Disconnected":
		{
            "name": "Chat - Disconnected",
            "sys_id": "92cb0337db4203c44ba3f9931d961990",
            "u_call_type": "Question",
			"u_queue": "Chat",
			"cmdb_ci": "547e7938ec93a500f47cd2fef70934fd",
			"short_description": "Chat - Disconnected",
			"work_notes": "User disconnected prior to responding to chat",
        },
        "3rdP Bb -  (Pearson MyEnglishLab)":
		{
            "name": "3rdP Bb -  (Pearson MyEnglishLab)",
            "sys_id": "989635c62b8e46004d7f717bf8da1568",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyEnglishLab)",
			"comments": "",
			"cmdb_ci": "b6a9c25cf5781900f47c48c0f722faa2",
        },
        "ASIST":
		{
            "name": "ASIST",
            "sys_id": "99aa7af52b394a004d7f717bf8da15ad",
            "short_description": "ASIST",
			"cmdb_ci": "e6d7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Outage - Bb":
		{
            "name": "Outage - Bb",
            "sys_id": "9a4c1dbf132aee803619b2776144b00b",
            "short_description": "Blackboard - Unresponsive/Not loading",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Account Access (Forgot Password)":
		{
            "name": "Account Access (Forgot Password)",
            "sys_id": "9a6ec81e2b31c60082f289efe8da15b9",
            "cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Account Access (Forgot Password)",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"call_type": "Password Reset",
        },
        "INCIDENT - Automatic PW Reset Error":
		{
            "name": "INCIDENT - Automatic PW Reset Error",
            "sys_id": "9ac9355e136fb6403619b2776144b08b",
            "u_call_type": "Incident",
			"u_queue": "T1 English",
			"parent": "603361c613ab3a401c19dc228144b0c0",
			"short_description": "Automatic Password Reset: \"Your request could not be processed\" - Error",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
        },
        "Sharepoint":
		{
            "name": "Sharepoint",
            "sys_id": "9caa7ef52b394a004d7f717bf8da15d1",
            "short_description": "Sharepoint (ISSUEORFORMNAME)",
			"cmdb_ci": "52d7b05c2ce31044b9a9c4f53e832aea",
			"comments": "",
        },
        "Account Access (Locked)":
		{
            "name": "Account Access (Locked)",
            "sys_id": "9cc1b4a32b3d02004d7f717bf8da157b",
            "short_description": "Account Access (Locked)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
        },
        "SSRS - Report/Server Issues ":
		{
            "name": "SSRS - Report/Server Issues ",
            "sys_id": "9d43c4ac6f1bca404508e90d2c3ee44f",
            "short_description": "SSRS - Report/Server Issues",
			"cmdb_ci": "1c2a70837917b0805af1d2eb7ec6bbdb",
			"comments": "",
        },
        "Outage -  Website/Network":
		{
            "name": "Outage -  Website/Network",
            "sys_id": "9ecdd0da13dc26c41c19dc228144b002",
            "short_description": "Outage - Financial Check-In",
			"cmdb_ci": "e6d7b05c2ce31044b9a9c4f53e832aeb",
			"u_call_type": "Incident",
        },
        "Transfer - To Another Department":
		{
            "name": "Transfer - To Another Department",
            "sys_id": "9f2836f12b394a004d7f717bf8da1552",
            "short_description": "Caller Needs Another Department",
			"cmdb_ci": "62d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
			"u_transfer": "true",
        },
        "3rdP Bb -  (Pearson MyLabsPlus)":
		{
            "name": "3rdP Bb -  (Pearson MyLabsPlus)",
            "sys_id": "a2eec965132d6a40935b7e076144b079",
            "short_description": "Blackboard - 3rd Party Integration (Pearson MyLabsPlus)",
			"comments": "",
			"cmdb_ci": "5f3b103835c0a5405af1cb6de5727fd1",
        },
        "Planned Banner Downtime":
		{
            "name": "Planned Banner Downtime",
            "sys_id": "a346cb392b794a004d7f717bf8da1547",
            "short_description": "Planned Banner Downtime",
			"cmdb_ci": "aad7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Campus Support / Walk-in":
		{
            "name": "Campus Support / Walk-in",
            "sys_id": "a48836352b394a004d7f717bf8da1574",
            "short_description": "Campus Support / Walk-in",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "Outlook":
		{
            "name": "Outlook",
            "sys_id": "a56fea352bf54a004d7f717bf8da1518",
            "cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"short_description": "Outlook",
			"comments": "",
        },
        "Device Support":
		{
            "name": "Device Support",
            "sys_id": "a638df842b4ac20082f289efe8da1579",
            "cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"short_description": "Device Support (DEVICETYPE)",
			"comments": "",
        },
        "Bb - Courses Not Displayed":
		{
            "name": "Bb - Courses Not Displayed",
            "sys_id": "a68536bd2bf54a004d7f717bf8da15e8",
            "short_description": "Blackboard - Courses Not Displayed",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "3rdP Bb - (Top Hat)":
		{
            "name": "3rdP Bb - (Top Hat)",
            "sys_id": "a7cf957e1321aa80935b7e076144b01c",
            "short_description": " Blackboard - 3rd Party Integration - (Top Hat)",
			"comments": "",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
        },
        "Xtender":
		{
            "name": "Xtender",
            "sys_id": "a98a7af52b394a004d7f717bf8da15fe",
            "short_description": "Xtender",
			"cmdb_ci": "aad7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Transfer - To T2":
		{
            "name": "Transfer - To T2",
            "sys_id": "aad1d1262bb10e004d7f717bf8da15e8",
            "short_description": "Transfer to T2",
			"u_transfer": "true",
			"u_transfer_internal": "true",
			"cmdb_ci": "8ed7b05c2ce31044b9a9c4f53e832ae9",
			"u_transfer_queue": "T2 English",
			"comments": "",
        },
        "Email - License Expired":
		{
            "name": "Email - License Expired",
            "sys_id": "abf2b83a13c4b6443619b2776144b0fb",
            "u_call_type": "Question",
			"u_complaint": "false",
			"u_firstcall_resolution": "false",
			"cmdb_ci": "fcfe3204a894e480b9a994d13108acc4",
			"short_description": "Mailbox Unavailable - License Expired",
        },
        "Email - Premised":
		{
            "name": "Email - Premised",
            "sys_id": "af68fef12b394a004d7f717bf8da1599",
            "short_description": "Premised Email",
			"cmdb_ci": "5e9064a70a0a3c9e00116ead66297e2e",
			"comments": "",
        },
        "Website Access - ISP Issues":
		{
            "name": "Website Access - ISP Issues",
            "sys_id": "aff3ca156f728200b075abcf9f3ee466",
            "short_description": "Website Access - ISP Issues",
			"cmdb_ci": "33667d1f0a0a3c9e01a6addbef08cbee",
			"comments": "",
        },
        "Bb - Grades / Feedback":
		{
            "name": "Bb - Grades / Feedback",
            "sys_id": "b27521f42b0e860082f289efe8da15cd",
            "short_description": "Blackboard - Grades / Feedback",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Website Access - Navigation/Questions":
		{
            "name": "Website Access - Navigation/Questions",
            "sys_id": "b3651a10133a16403619b2776144b062",
            "short_description": "Website Access - Navigation/Questions",
			"cmdb_ci": "33667d1f0a0a3c9e01a6addbef08cbee",
			"comments": "",
        },
        "Office 365 (Software)":
		{
            "name": "Office 365 (Software)",
            "sys_id": "b424b63d2bf54a004d7f717bf8da15ff",
            "short_description": "Office 365 (Software)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "External WebApp - FOCUS 2":
		{
            "name": "External WebApp - FOCUS 2",
            "sys_id": "b77836352b394a004d7f717bf8da15aa",
            "short_description": "External WebApp - FOCUS 2",
			"cmdb_ci": "33667d1f0a0a3c9e01a6addbef08cbee",
			"comments": "",
        },
        "Lync (Install/Use)":
		{
            "name": "Lync (Install/Use)",
            "sys_id": "bb547e3d2bf54a004d7f717bf8da1551",
            "short_description": "Lync (Install/Use)",
			"cmdb_ci": "ead7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "External - Work From Home System Check":
		{
            "name": "External - Work From Home System Check",
            "sys_id": "be7fb5a96ff4d600b075abcf9f3ee4fb",
            "short_description": "External - Work From Home System Check",
			"comments": "",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
        },
        "Desktop - LU":
		{
            "name": "Desktop - LU",
            "sys_id": "c10db6f92b394a004d7f717bf8da154d",
            "short_description": "LU Desktop (COMPUTERNAME)",
			"cmdb_ci": "e2d7b05c2ce31044b9a9c4f53e832aec",
			"comments": "",
        },
        "Account Access (Expired - Inactive)":
		{
            "name": "Account Access (Expired - Inactive)",
            "sys_id": "c47c882d131716c01c19dc228144b03b",
            "short_description": "Account Access (Expired - Inactive)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"u_call_type": "Password Reset",
        },
        "3rdP Bb - (BreakingGround)":
		{
            "name": "3rdP Bb - (BreakingGround)",
            "sys_id": "c5896eca2b0686004d7f717bf8da15c0",
            "short_description": "Bb - 3rd Party (BreakingGround)",
			"comments": "",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
        },
        "Other":
		{
            "name": "Other",
            "sys_id": "c65195e22bb10e004d7f717bf8da1578",
            "short_description": "Other - DESCRIBEISSUEIN2OR3WORDS",
			"comments": "",
        },
        "Telephony  - Soft Phone":
		{
            "name": "Telephony  - Soft Phone",
            "sys_id": "c80db6f92b394a004d7f717bf8da154b",
            "short_description": "Soft Phone (EXTENSION/COMPUTER NAME)",
			"cmdb_ci": "33667c8b0a0a3c9e002cd01637dad6b0",
			"comments": "",
        },
        "Account Access (Wrong/Forgot Username)":
		{
            "name": "Account Access (Wrong/Forgot Username)",
            "sys_id": "cb5bd0d7137d2a401c19dc228144b03d",
            "cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Account Access (Wrong/Forgot Username)",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
        },
        "Molly / FS2":
		{
            "name": "Molly / FS2",
            "sys_id": "cb9eba7d2b394a004d7f717bf8da152f",
            "short_description": "Molly/FS2",
			"cmdb_ci": "e6d7b05c2ce31044b9a9c4f53e832aed",
			"comments": "",
        },
        "Telephony - Hard Phone":
		{
            "name": "Telephony - Hard Phone",
            "sys_id": "cf0db6f92b394a004d7f717bf8da150e",
            "short_description": "Hard Phone (EXTENSION/MAC)",
			"cmdb_ci": "33667c8b0a0a3c9e002cd01637dad6b0",
			"comments": "",
        },
        "Account Access (Expired - Compromised)":
		{
            "name": "Account Access (Expired - Compromised)",
            "sys_id": "d07d0cad131716c01c19dc228144b085",
            "short_description": "Account Access (Expired - Compromised)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "The steps and links listed in the following article will allow you to reset your password and set up Automated Password Reset, so that you can reset your password through a verified phone number or email address: [code]<a href=\"https://liberty.service-now.com/kb_view.do?sysparm_article=KB0013432\">Click Here</a>[/code]\r\n\r\nIf you experience any issues with this process, feel free to give us a call back.\r\n\r\nThank you,\r\nIT HelpDesk",
			"u_call_type": "Password Reset",
        },
        "External - Work From Home [Issue]":
		{
            "name": "External - Work From Home [Issue]",
            "sys_id": "d19eba7d2b394a004d7f717bf8da150b",
            "short_description": "External - Work From Home (ISSUE)",
			"comments": "",
        },
        "Bb - Assignment Submission":
		{
            "name": "Bb - Assignment Submission",
            "sys_id": "d72596682b1a464082f289efe8da15b9",
            "short_description": "Blackboard - Assignment Submission",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Username Change Inquiry":
		{
            "name": "Username Change Inquiry",
            "sys_id": "d91657942b8a060082f289efe8da15e6",
            "cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Username Change Inquiry",
			"comments": "",
        },
        "Account Access (Expired - FR Alert)":
		{
            "name": "Account Access (Expired - FR Alert)",
            "sys_id": "db94b71d13f6e2481c19dc228144b0be",
            "short_description": "Account Access (Expired - FR Alert)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "Thank you for contacting the Liberty University IT HelpDesk. \r\n\r\nIn order to regain access to your Liberty Network Account, please send an email to: \r\n\r\nFARedFlag@liberty.edu \r\n\r\nOur Financial Aid department will then work with you to resolve any issues with your account.",
        },
        "Work From Home Systems Check":
		{
            "name": "Work From Home Systems Check",
            "sys_id": "dc1feaf12bf54a004d7f717bf8da15d5",
            "short_description": "Work From Home Systems Check",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "3rdP Bb - (Rosetta Stone)":
		{
            "name": "3rdP Bb - (Rosetta Stone)",
            "sys_id": "dc892eca2b0686004d7f717bf8da1596",
            "short_description": "Blackboard - 3rd Party Integration (Rosetta Stone)",
			"comments": "",
			"cmdb_ci": "23ebb1f0681f65005af19b008fde205d",
        },
        "External - Medical Clinic":
		{
            "name": "External - Medical Clinic",
            "sys_id": "dc9eba7d2b394a004d7f717bf8da156b",
            "short_description": "Medical Clinic (ISSUE)",
			"comments": "",
        },
        "Outage - External":
		{
            "name": "Outage - External",
            "sys_id": "ddb5d75c135df6401c19dc228144b06b",
            "short_description": "Outage - Amazon AWS S3",
			"cmdb_ci": "33667d1f0a0a3c9e01a6addbef08cbee",
			"parent": "33d31f1c1359b2003619b2776144b099",
			"u_call_type": "Incident",
        },
        "Forms":
		{
            "name": "Forms, other",
            "sys_id": "de019410db568fc489b6fd561d9619c7",
			"u_call_type": "Question",
			"cmdb_ci": "a6d7b05c2ce31044b9a9c4f53e832aec",
			"short_description": "Forms (name and/or type of form)",
        },
        "Office 2016 Migration - Mac":
		{
            "name": "Office 2016 Migration - Mac",
            "sys_id": "de4408e6135ffe481c19dc228144b014",
            "u_call_type": "Question",
			"u_operating_system": "OS X",
			"short_description": "Office 2016 Migration Issues - Mac",
			"comments": "Users that have not migrated their mailboxes to 2016 already will need to map their mailbox in 2016 after the deployment.\r\n\r\n\"How do I import my Office profile form Outlook 2011 to Outlook 2016?\"\r\nhttps://liberty.service-now.com/kb_view.do?sysparm_article=KB0016003",
			"parent": "9cec776713ba72c01c19dc228144b0bb",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
        },
        "Books / Materials (MBS Direct)":
		{
            "name": "Books / Materials (MBS Direct)",
            "sys_id": "df45f2bd2bf54a004d7f717bf8da1541",
            "short_description": "Books / Materials (MBS Direct)",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Authorize.net":
		{
            "name": "Authorize.net",
            "sys_id": "e15a607d13da22441c19dc228144b019",
            "short_description": "Authorize.net",
			"cmdb_ci": "632b0cb5386c30007046847eea261b81",
			"comments": "",
        },
        "Civitas - Password Reset":
		{
            "name": "Civitas - Password Reset",
            "sys_id": "e2c1d3f06f6f0a804508e90d2c3ee48a",
            "short_description": "Civitas - Password Reset",
			"cmdb_ci": "f4407c732b39420082f289efe8da15bc",
			"comments": "",
			"u_call_type": "Password Reset",
        },
        "Call Handling - Finesse Failure":
		{
            "name": "Call Handling - Finesse Failure",
            "sys_id": "e59d6478131947841c19dc228144b023",
            "short_description": "Call Handling - HDRS Finesse Failure",
			"cmdb_ci": "8ed7b05c2ce31044b9a9c4f53e832ae9",
        },
        "Software Installation":
		{
            "name": "Software Installation",
            "sys_id": "e7506d2b2b7d02004d7f717bf8da15e7",
            "short_description": "Software Installation (NAMEOFSOFTWARE)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "3rdP Bb - (McGraw Hill / SIMNet)":
		{
            "name": "3rdP Bb - (McGraw Hill / SIMNet)",
            "sys_id": "e8c0bd0e2b4e46004d7f717bf8da1596",
            "short_description": "Blackboard - 3rd Party Integration (McGraw Hill / SIMNet)",
			"comments": "",
			"cmdb_ci": "d559861cf5781900f47c48c0f722faef",
        },
        "Software Troubleshooting":
		{
            "name": "Software Troubleshooting",
            "sys_id": "eca06d2b2b7d02004d7f717bf8da15f1",
            "short_description": "Software Troubleshooting (NAMEOFSOFTWARE)",
			"cmdb_ci": "aed7b05c2ce31044b9a9c4f53e832aeb",
			"comments": "",
        },
        "Bb - Course Content Issue":
		{
            "name": "Bb - Course Content Issue",
            "sys_id": "eccb99b02bca860082f289efe8da152a",
            "short_description": "Blackboard - Course Content Issue",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Training - transfer to General Queue":
		{
            "name": "Training - transfer to General Queue",
            "sys_id": "efcaba1bdbfdcf4089b6fd561d96191e",
            "u_call_type": "Question",
			"u_firstcall_resolution": "false",
			"u_queue": "T1 English",
			"u_transfer": "true",
			"u_transfer_internal": "true",
			"u_transfer_list": "9916ce750a0a3c9e01bc78b1c8da0678",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"short_description": "Training - transfer to General Queue",
        },
        "Pilot - Blackboard Landing Page ":
		{
            "name": "Pilot - Blackboard Landing Page ",
            "sys_id": "f0d6e1a8136d6240935b7e076144b088",
            "short_description": "Pilot - Blackboard Landing Page ",
			"comments": "",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
        },
        "CRM":
		{
            "name": "CRM",
            "sys_id": "f29a3af52b394a004d7f717bf8da15ee",
            "short_description": "CRM",
			"cmdb_ci": "3366788c0a0a3c9e01cca6a23490f09f",
			"comments": "",
        },
        "IT Marketplace (Site/Navigation)":
		{
            "name": "IT Marketplace (Site/Navigation)",
            "sys_id": "f78abaf52b394a004d7f717bf8da158f",
            "short_description": "IT Marketplace (Site/Navigation)",
			"cmdb_ci": "4846b65b9c48ad40f47c19537d850c22",
			"comments": "",
        },
        "3rdP Bb - (Creation Curriculum CRST290)":
		{
            "name": "3rdP Bb - (Creation Curriculum CRST290)",
            "sys_id": "fbba3f6113921e403619b2776144b00d",
            "short_description": "Blackboard - 3rd Party Integration (Creation Curriculum CRST290)",
			"comments": "",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
        },
        "Bb - Presentation Videos":
		{
            "name": "Bb - Presentation Videos",
            "sys_id": "fd477e712b394a004d7f717bf8da1589",
            "short_description": "Blackboard - Presentation Videos",
			"cmdb_ci": "33667a690a0a3c9e018978b21a45faf2",
			"comments": "",
        },
        "Permissions Request":
		{
            "name": "Permissions Request",
            "sys_id": "fedbbe792b394a004d7f717bf8da156c",
            "short_description": "Permissions Request (PERMISSIONTYPE)",
			"cmdb_ci": "26d7b05c2ce31044b9a9c4f53e832aef",
			"comments": "",
			"u_call_type": "Request",
        },
        "Sharepoint Security Update / Access":
		{
            "name": "Sharepoint Security Update / Access",
            "sys_id": "ffe6a28f2b2ac6804d7f717bf8da15d2",
            "short_description": "Sharepoint Security Update / Access",
			"cmdb_ci": "52d7b05c2ce31044b9a9c4f53e832aea",
			"parent": "1a80189f13aa52403619b2776144b073",
        }
}
